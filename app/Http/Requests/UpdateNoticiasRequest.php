<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Noticias;

class UpdateNoticiasRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Noticias::$rules_update;
    }

    public function messages()
    {
        return[
            'titulo.required'    => 'El campo <u>Titulo</u> es obligatorio',
            'sub_titulo.required'             => 'El campo <u>Sub Titulo</u> es obligatorio',
            'url.required'            => 'El campo <u>URL</u> es obligatorio',
            'descripcion.required'            => 'El campo <u>Descripción</u> es obligatorio'
        ];
    }
}
