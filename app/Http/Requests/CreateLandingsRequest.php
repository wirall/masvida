<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Landings;

class CreateLandingsRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Landings::$rules;
    }

    public function messages()
    {
        return[
            'titulo.required'    => 'El campo <u>Titulo</u> es obligatorio',
            'texto.required'             => 'El campo <u>Texto</u> es obligatorio',
            'texto_caducidad.required'    => 'El campo <u>Texto caducidad</u> es obligatorio',
            'imagen.required'            => 'El campo <u>Imagen</u> es obligatorio',
            'inicio.required'            => 'El campo <u>Inicio</u> es obligatorio',
            'fin.required'            => 'El campo <u>Fin</u> es obligatorio',
            'url.required'            => 'El campo <u>Url</u> es obligatorio',
            'url.unique'            => 'El campo <u>URL</u> ya esta en uso.',
            'servicios.required'            => 'El campo <u>Servicios</u> es obligatorio'
        ];
    }
}
