<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CvFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
	    	'nombre' => 'required',
	    	'apellido' => 'required',
	    	'puesto' => 'required',
	    	'departamento' => 'required',	    	
	    	'direccion' => 'required',
	    	'localidad' => 'required',
	    	'telefono' => 'required|digits_between:8,16',
	    	'email' => 'required|email',
	    	'carta' => 'required',
	    	'matriculado' => 'required',
	    	'cv' => 'required|max:20000|mimes:txt,pdf,doc,docx',
            'recaptcha-response'  => 'required'
	  	];
	}

	public function messages()
    {
        return [
	    	'nombre.required' => 'Ingrese su nombre',
	    	'apellido.required' => 'Ingrese su apellido',
	    	'puesto.required' => 'Ingrese el puesto al cual quiere aplicar',
	    	'departamento.required' => 'Ingrese el departamento al cual quiere aplicar',
	    	'direccion.required' => 'Ingrese su dirección',
	    	'localidad.required' => 'Ingrese su localidad',
	    	'telefono.required' => 'Ingrese su número de teléfono',
	    	'telefono.digits_between' => 'El teléfono debe contener al menos 8 caracteres numéricos',
	    	'email.required' => 'Ingrese su dirección de email',
	    	'email.email' => 'Ingrese un email válido',
	    	'carta.required' => 'Escriba una carta de presentación',
	    	'matriculado.required' => 'Indique si es o no profesional matriculado',
	    	'cv.max' => 'El CV no debe superar los 2MB',
	    	'cv.mimes' => 'El CV debe ser .txt, .pdf o .doc',
	    	'cv.required' => 'Cargue su CV',
            'recaptcha-response' => 'reCAPTCHA inválido',
	  	];
    }

}
