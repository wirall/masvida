<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Noticias;

class CreateNoticiasRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Noticias::$rules;
    }

    public function messages()
    {
        return[
            'titulo.required'    => 'El campo <u>Titulo</u> es obligatorio',
            'sub_titulo.required'             => 'El campo <u>Sub Titulo</u> es obligatorio',
            'url.required'            => 'El campo <u>URL</u> es obligatorio',
            'url.unique'            => 'El campo <u>URL</u> ya esta en uso.',
            'descripcion.required'            => 'El campo <u>Descripción</u> es obligatorio',
            'image.required'            => 'El campo <u>Imagen</u> es obligatoria',
            'image_thumb.required'            => 'El campo <u>Imagen de miniatura</u> es obligatoria'
        ];
    }
}
