<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class FamiliaFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [	    	
	    	'nombre' => 'required',
	    	'apellido' => 'required',
	    	'telefono' => 'required|digits_between:8,16',
	    	'email' => 'required|email',
	    	'cantidad' => 'numeric',
            'recaptcha-response'  => 'required'
	  	];
	}

	public function messages()
    {
        return [
	        'nombre.required' => 'Ingrese su nombre', 
	        'nombre.min' => 'Su nombre debe contener al menos 3 caracteres', 
	        'apellido.required' => 'Ingrese su apellido',
	        'apellido.min' => 'Su apellido debe contener al menos 3 caracteres',
	        'telefono.required' => 'Ingrese su teléfono',
	        'telefono.digits_between' => 'El teléfono debe contener al menos 8 caracteres numéricos',
	        'email.required' => 'Ingrese su email',
	        'email.email' => 'Ingrese un email válido',
	        'cantidad.numeric' => 'Debe ser un valor numérico',
            'recaptcha-response' => 'reCAPTCHA inválido',
        ];
    }

}
