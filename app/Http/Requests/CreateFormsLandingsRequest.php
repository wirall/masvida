<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\FormsLandings;

class CreateFormsLandingsRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return FormsLandings::$rules;
    }

    public function messages()
    {
        return[
            'nombre.required'    => 'El campo <u>Nombre</u> es obligatorio',
            'apellido.required'             => 'El campo <u>Apellido</u> es obligatorio',
            'telefono.required'    => 'El campo <u>Teléfono</u> es obligatorio',
            'email.required'            => 'El campo <u>Email</u> es obligatorio',
            'email.email'            => 'Ingrese un <u>Email</u> válido'
        ];
    }
}
