<?php

namespace App\Http\Controllers\Auth;

use Mail;
use App\User;
use Validator;
use App\Http\Controllers\Auth\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    // protected $loginPath = '/cv';
		// protected $redirectTo = '/cv';
		// protected $redirectAfterLogout = '/home';

    /**
		 * Get the post register / login redirect path.
		 *
		 * @return string
		 */
		public function redirectPath()
		{

		    // Logic that determines where to send the user
		    if (\Auth::user()->role_id == 1) {
		        return '/admin';
		    }
		    // Logic that determines where to send the user
		    if (\Auth::user()->role_id == 2) {
		        return '/admin';
		    }
		    // Logic that determines where to send the user
		    if (\Auth::user()->role_id == 3) {
		        return '/admin';
		    }
		    // Logic that determines where to send the user
		    if (\Auth::user()->role_id == 4) {
		        return '/admin';
		    }
		    // Logic that determines where to send the user
		    if (\Auth::user()->role_id == 5) {
		        return '/';
		    		// return Redirect::to(URL::previous());
		    }
		}
		
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        // dd( $data['_token'] );
        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role_id' => 5
        ]);
         
    }

}
