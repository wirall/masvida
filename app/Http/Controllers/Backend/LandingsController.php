<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\CreateLandingsRequest;
use App\Http\Requests\UpdateLandingsRequest;
use App\Repositories\LandingsRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use InfyOm\Generator\Controller\AppBaseController;
use App\Models\Landings;
use Input;
use File;

class LandingsController extends AppBaseController
{
    /** @var  LandingsRepository */
    private $landingsRepository;
    private $path;
    private $servicios;

    public function __construct(LandingsRepository $landingsRepo)
    {
        $this->landingsRepository = $landingsRepo;
        $this->path = public_path().'/uploads/landings/';
        $this->servicios  = array('Medicina Laboral', 'Área Protegida', 'Área Cardio', 'Cursos', 
                                'Servicio de Médico y Enfermería en Planta', 
                                'Eventos', 'Internación Domiciliaria', 'Novedades', 
                                'Recursos Humanos', 'Convenio y Planes para Empleados', 'Familia');
    }

    /**
     * Display a listing of the Landings.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->landingsRepository->pushCriteria(new RequestCriteria($request));
        $landings = $this->landingsRepository->all();

        return view('backend.landings.index')
            ->with('landings', $landings);
    }

    /**
     * Show the form for creating a new Landings.
     *
     * @return Response
     */
    public function create()
    {
        $servicios_list = $this->servicios;
        return view('backend.landings.create')->with('servicios_list', $servicios_list);
    }

    /**
     * Store a newly created Landings in storage.
     *
     * @param CreateLandingsRequest $request
     *
     * @return Response
     */
    public function store(CreateLandingsRequest $request)
    {
        // Si no existe la carpeta la crea la primera vez
        if (!file_exists(public_path().'/uploads/landings/')) {
          mkdir(public_path().'/uploads/landings/', 0777, true);
        }

        // Genero Imagen
        $newName = app('App\Http\Controllers\Backend\FilesRenamerController')->rename( $request->file('imagen') , '_land.' );

        // Guarda Imagen
        $request->file('imagen')->move( $this->path , $newName ); 
        
        $landing = Landings::create([
            'titulo' => $request->input('titulo'),
            'texto' => $request->input('texto'),
            'texto_caducidad' => $request->input('texto_caducidad'),
            'imagen' => $newName, 
            'inicio' => $request->input('inicio'),
            'fin' => $request->input('fin'),
            'url' => $request->input('url'),
            'servicios' => $request->input('servicios'),
            'servicios' => $request->input('servicios'),
        ]);

        Flash::success('Landing creada con éxito.');
        return redirect(route('landings.index'));
    }

    /**
     * Display the specified Landings.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $landings = $this->landingsRepository->findWithoutFail($id);

        if (empty($landings)) {
            Flash::error('Landings not found');

            return redirect(route('landings.index'));
        }

        return view('backend.landings.show')->with('landings', $landings);
    }

    /**
     * Show the form for editing the specified Landings.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $landings = $this->landingsRepository->findWithoutFail($id);

        if (empty($landings)) {
            Flash::error('Landings not found');

            return redirect(route('landings.index'));
        }

        $servicios_list = $this->servicios;
        return view('backend.landings.edit')->with('landings', $landings)->with('servicios_list', $servicios_list);

    }

    /**
     * Update the specified Landings in storage.
     *
     * @param  int              $id
     * @param UpdateLandingsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLandingsRequest $request)
    {

        $landings = $this->landingsRepository->findWithoutFail($id);
        if (empty($landings)) {
            Flash::error('Landing not found');
            return redirect(route('landings.index'));
        }

        if ( $request->hasFile('imagen') ) {
            $newName = app('App\Http\Controllers\Backend\FilesRenamerController')->rename( $request->file('imagen') , '_land.' );
            $request->file('imagen')->move( $this->path , $newName ); 
            File::delete( $this->path.$landings->imagen );
            $update = $this->landingsRepository->update(['imagen' => $newName], $id);
        }

        $update = $this->landingsRepository->update([
            'titulo' => $request->input('titulo'),
            'texto' => $request->input('texto'),
            'texto_caducidad' => $request->input('texto_caducidad'),
            'inicio' => $request->input('inicio'),
            'fin' => $request->input('fin'),
            'url' => $request->input('url'),
            'servicios' => $request->input('servicios')
            ], $id)
        ;

        Flash::success('Landing editada con éxito.');
        return redirect(route('landings.index'));
    }

    /**
     * Remove the specified Landings from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $landings = $this->landingsRepository->findWithoutFail($id);

        if (empty($landings)) {
            Flash::error('Landings not found');

            return redirect(route('landings.index'));
        }

        $this->landingsRepository->delete($id);

        Flash::success('Landings eliminada con éxito.');

        return redirect(route('landings.index'));
    }
}
