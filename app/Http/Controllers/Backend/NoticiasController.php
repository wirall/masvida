<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\CreateNoticiasRequest;
use App\Http\Requests\UpdateNoticiasRequest;
use App\Repositories\NoticiasRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Noticias;
use File;

class NoticiasController extends AppBaseController
{
    /** @var  NoticiasRepository */
    private $noticiasRepository;
    private $path;

    public function __construct(NoticiasRepository $noticiasRepo)
    {
        $this->noticiasRepository = $noticiasRepo;
        $this->path = public_path().'/uploads/noticias/';
    }

    /**
     * Display a listing of the Noticias.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->noticiasRepository->pushCriteria(new RequestCriteria($request));
        $noticias = $this->noticiasRepository->orderBy('id', 'desc')->all();

        return view('backend.noticias.index')
            ->with('noticias', $noticias);
    }

    /**
     * Show the form for creating a new Noticias.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.noticias.create');
    }

    /**
     * Store a newly created Noticias in storage.
     *
     * @param CreateNoticiasRequest $request
     *
     * @return Response
     */
    public function store(CreateNoticiasRequest $request)
    {
        // Si no existe la carpeta la crea la primera vez
        if (!file_exists(public_path().'/uploads/noticias/')) {
          mkdir(public_path().'/uploads/noticias/', 0777, true);
        }

        // Genera Imagen
        $newNameImage = app('App\Http\Controllers\Backend\FilesRenamerController')->rename( $request->file('image') , '_news.' );
        // Guarda Imagen
        $request->file('image')->move( $this->path , $newNameImage ); 
        // Genera Imagen_thumb
        $newNameThumb = app('App\Http\Controllers\Backend\FilesRenamerController')->rename( $request->file('image_thumb') , '_thumb_news.' );
        // Guarda Imagen_thumb
        $request->file('image_thumb')->move( $this->path , $newNameThumb ); 

        $root = Noticias::create([
            'titulo' => $request->input('titulo'),
            'sub_titulo' => $request->input('sub_titulo'),
            'descripcion' => $request->input('descripcion'),
            'image' => $newNameImage, 
            'image_thumb' => $newNameThumb, 
            'url' => $request->input('url')
        ]);

        Flash::success('Noticia creada con éxito.');
        return redirect(route('noticias.index'));
    }

    /**
     * Display the specified Noticias.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $noticias = $this->noticiasRepository->findWithoutFail($id);

        if (empty($noticias)) {
            Flash::error('Noticias not found');

            return redirect(route('noticias.index'));
        }

        return view('backend.noticias.show')->with('noticias', $noticias);
    }

    /**
     * Show the form for editing the specified Noticias.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $noticias = $this->noticiasRepository->findWithoutFail($id);

        if (empty($noticias)) {
            Flash::error('Noticias not found');

            return redirect(route('noticias.index'));
        }

        return view('backend.noticias.edit')->with('noticias', $noticias);
    }

    /**
     * Update the specified Noticias in storage.
     *
     * @param  int              $id
     * @param UpdateNoticiasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNoticiasRequest $request)
    {
        $noticias = $this->noticiasRepository->findWithoutFail($id);

        if (empty($noticias)) {
            Flash::error('Noticias not found');

            return redirect(route('noticias.index'));
        }

        if ( $request->hasFile('image') ) {
            $newNameImage = app('App\Http\Controllers\Backend\FilesRenamerController')->rename( $request->file('image') , '_news.' );
            $request->file('image')->move( $this->path , $newNameImage ); 
            File::delete( $this->path.$noticias->image );
            $update = $this->noticiasRepository->update(['image' => $newNameImage], $id);
        }
        if ( $request->hasFile('image_thumb') ) {
            $newNameThumb = app('App\Http\Controllers\Backend\FilesRenamerController')->rename( $request->file('image_thumb') , '_thumb_news.' );
            $request->file('image_thumb')->move( $this->path , $newNameThumb ); 
            File::delete( $this->path.$noticias->image_thumb );
            $update = $this->noticiasRepository->update(['image_thumb' => $newNameThumb], $id);
        }

        $update = $this->noticiasRepository->update([
            'titulo' => $request->input('titulo'),
            'sub_titulo' => $request->input('sub_titulo'),
            'descripcion' => $request->input('descripcion'),
            'url' => $request->input('url')
            ], $id)
        ;

        Flash::success('Noticia editada con éxito.');

        return redirect(route('noticias.index'));
    }

    /**
     * Remove the specified Noticias from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $noticias = $this->noticiasRepository->findWithoutFail($id);

        if (empty($noticias)) {
            Flash::error('Noticias not found');

            return redirect(route('noticias.index'));
        }

        $this->noticiasRepository->delete($id);

        Flash::success('Noticia eliminada con éxito.');

        return redirect(route('noticias.index'));
    }
}
