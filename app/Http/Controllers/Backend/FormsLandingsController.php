<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\CreateFormsLandingsRequest;
use App\Http\Requests\UpdateFormsLandingsRequest;
use App\Repositories\FormsLandingsRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use InfyOm\Generator\Controller\AppBaseController;
use App\Models\Landings;
use DB;
use Mail;

class FormsLandingsController extends AppBaseController
{
    /** @var  FormsLandingsRepository */
    private $formsLandingsRepository;
    //protected $clientEmail = 'giannina@wirallinteractive.com.ar';
    //protected $clientSendEmail = 'giannina@wirallinteractive.com.ar';
    protected $clientEmail = 'contacto@masvida.com.ar';
    protected $clientSendEmail = 'contacto@masvida.com.ar';

    public function __construct(FormsLandingsRepository $formsLandingsRepo)
    {
        $this->formsLandingsRepository = $formsLandingsRepo;
    }

    /**
     * Display a listing of the FormsLandings.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->formsLandingsRepository->pushCriteria(new RequestCriteria($request));
        //$formsLandings = $this->formsLandingsRepository->all();

        $formsLandings = DB::table('landings') 
            ->join('forms_landings', 'landings.id', '=', 'forms_landings.id_landing')
            ->where('forms_landings.deleted_at', '=', NULL)
            ->select('forms_landings.id','forms_landings.created_at AS fechaFormsLandings','forms_landings.nombre', 
                    'forms_landings.apellido','forms_landings.email',  'forms_landings.telefono', 'landings.titulo AS tituloLanding')
            ->get();

        return view('backend.formsLandings.index')
            ->with('formsLandings', $formsLandings);
    }

    /**
     * Show the form for creating a new FormsLandings.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.formsLandings.create');
    }

    /**
     * Store a newly created FormsLandings in storage.
     *
     * @param CreateFormsLandingsRequest $request
     *
     * @return Response
     */
    public function store(CreateFormsLandingsRequest $request)
    {
        $input = $request->all();

        $formsLandings = $this->formsLandingsRepository->create($input);

        Flash::success('Mensaje enviado con éxito.');

        $landings = Landings::where('id' , '=', $request->input('id_landing'))->first();

        $inicio = strtotime($landings->inicio); 
        $fin = strtotime($landings->fin); 
        $fecha = strtotime(date("Y-m-d")); 
         
        if ($fecha >= $inicio && $fecha <= $fin ) {
            $activo = true;
        } else {
            $activo = false;
        }

        // Data message
        $data = array(
            'email'     => $formsLandings->email,
            'subject'   => 'Contacto desde el sitio +Vida (Landing)',
            //'to'            => 'giannina@wirallinteractive.com.ar'
            'to'            => 'contacto@masvida.com.ar'
        );
        // Mail to Mas Vida
        $htmlData = array(
            'landings' => $landings,
            'nombre'   => $formsLandings->nombre,
            'apellido' => $formsLandings->apellido,
            'telefono' => $formsLandings->telefono,
            'email'  => $formsLandings->email
        );

        Mail::send('emails.new-contact-landing', $htmlData, function ($message) use ($data){
          $message->from($data['email'], 'Mas vida Website');
          $message->to( $data['to'] , 'Mas Vida' )->subject($data['subject']);
        });

        return view('partials.landing')->with('landings', $landings)->with('activo', $activo);
    }

    /**
     * Display the specified FormsLandings.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $formsLandings = $this->formsLandingsRepository->findWithoutFail($id);

        if (empty($formsLandings)) {
            Flash::error('FormsLandings not found');

            return redirect(route('formsLandings.index'));
        }

        $landing = Landings::where('id' , '=', $formsLandings->id_landing)->first();
        return view('backend.formsLandings.show')->with('formsLandings', $formsLandings)->with('landing', $landing);
    }

    /**
     * Show the form for editing the specified FormsLandings.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $formsLandings = $this->formsLandingsRepository->findWithoutFail($id);

        if (empty($formsLandings)) {
            Flash::error('FormsLandings not found');

            return redirect(route('formsLandings.index'));
        }

        return view('backend.formsLandings.edit')->with('formsLandings', $formsLandings);
    }

    /**
     * Update the specified FormsLandings in storage.
     *
     * @param  int              $id
     * @param UpdateFormsLandingsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFormsLandingsRequest $request)
    {
        $formsLandings = $this->formsLandingsRepository->findWithoutFail($id);

        if (empty($formsLandings)) {
            Flash::error('FormsLandings not found');

            return redirect(route('formsLandings.index'));
        }

        $formsLandings = $this->formsLandingsRepository->update($request->all(), $id);

        Flash::success('FormsLandings updated successfully.');
        return redirect(route('formsLandings.index'));
    }

    /**
     * Remove the specified FormsLandings from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $formsLandings = $this->formsLandingsRepository->findWithoutFail($id);

        if (empty($formsLandings)) {
            Flash::error('FormsLandings not found');

            return redirect(route('formsLandings.index'));
        }

        $this->formsLandingsRepository->delete($id);

        Flash::success('Mensaje eliminado con éxito');

        return redirect(route('formsLandings.index'));
    }
}
