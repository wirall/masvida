<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Banners;
use Input;
use File;

class FilesRenamerController extends AppBaseController
{

    /**
     * Remove the specified Banners from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function rename( $fileToRename , $type )
    {
    		$file										= $fileToRename;
        $fileOriginalExtension 	= $file->getClientOriginalExtension(); 
        $fileNewName  					= time() . $type . $fileOriginalExtension;
        return $fileNewName;
    }
}
