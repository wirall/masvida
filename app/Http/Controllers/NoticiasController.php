<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateNoticiasRequest;
use App\Http\Requests\UpdateNoticiasRequest;
use App\Repositories\NoticiasRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Noticias;
use File;
use DB;

class NoticiasController extends AppBaseController
{
    /** @var  NoticiasRepository */
    private $noticiasRepository;
    private $path;

    public function __construct(NoticiasRepository $noticiasRepo)
    {
        $this->noticiasRepository = $noticiasRepo;
        $this->path = public_path().'/uploads/noticias/';
    }

    /**
     * Display a listing of the Noticias.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->noticiasRepository->pushCriteria(new RequestCriteria($request));
        $noticias = DB::table('noticias')->where('deleted_at', '=', NULL)->orderBy('id', 'desc')->paginate(8);
        $order = 'desc';

        foreach ($noticias as $not) {
            $not->descripcionTextoPlano = strip_tags($not->descripcion);
        }

        return view('partials.noticias')
            ->with('noticias', $noticias)
            ->with('order', $order);
    }


    /** 
     * Display the specified Noticias.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($slug)
    {

        $noticia = Noticias::where('url', $slug)->first(); 
        if (empty($noticia)) {
            Flash::error('Noticias not found');
            return redirect('/novedades');
        }
        $noticias = Noticias::where('id', '!=', $noticia->id)->orderBy('id', 'desc')->paginate(4)->all();

        return view('partials.noticia')->with('noticia', $noticia)->with('noticias', $noticias);
    }

    /**
     * Display a listing of the Features.
     *
     */
    public function showOrder($order)
    {
        $noticias = DB::table('noticias')->where('deleted_at', '=', NULL)->orderBy('id', $order)->paginate(8);

        foreach ($noticias as $not) {
            $not->descripcionTextoPlano = strip_tags($not->descripcion);
        }

        return view('partials.noticias')
            ->with('noticias', $noticias)
            ->with('order', $order);
    }
  

}
