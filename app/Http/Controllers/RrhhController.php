<?php 

namespace App\Http\Controllers;
use Request;
use App\Http\Requests\CvFormRequest;
use App\Rrhh;
use Mail;

class RrhhController extends Controller {

	public function index()
	{
		return view('partials.rrhh');
	}
	public function store(CvFormRequest $request){

		$nombre 				= Request::input('nombre');
		$apellido 			= Request::input('apellido');
		$puesto 				= Request::input('puesto');
		$departamento 	= Request::input('departamento');
		$direccion 			= Request::input('direccion');
		$localidad 			= Request::input('localidad');
		$telefono 			= Request::input('telefono');
		$email 					= Request::input('email');
		$matriculado 		= Request::input('matriculado');
		$carta 					= Request::input('carta');
    $file_renamed 	= "";
    if ( Request::hasFile('cv') ) { 
        $file 					= Request::file('cv');
        $file_original 	= $file->getClientOriginalName();
        $file_renamed 	= time() . '-' . $file_original;
        $file_path 			= public_path() . '/upload/cvs/';
        $file->move($file_path, $file_renamed);
    }
		
		$rrhh = new Rrhh;
		$rrhh->nombre 			= $nombre;
		$rrhh->apellido			= $apellido;
		$rrhh->puesto 			= $puesto;
		$rrhh->departamento	= $departamento;
		$rrhh->direccion 		= $direccion;
		$rrhh->localidad		= $localidad;
		$rrhh->telefono 		= $telefono;
		$rrhh->email 				= $email;
		$rrhh->matriculado	= $matriculado;
		$rrhh->carta 				= $carta;
		$rrhh->cv 					= $file_renamed;

		if( $rrhh->save() ){

			// Data message
			$data = array(
				'email'		=> $email,
				'subject'	=> 'Contacto desde el sitio +Vida (RRHH)',
				 'to'			=> array('contacto@masvida.com.ar', 'rrhh@masvida.com.ar')
			);

			// the data that will be passed into the mail view blade template
			$htmlData = array(
				'nombre'				=> $nombre,
				'apellido'			=> $apellido,
				'puesto'				=> $puesto,
				'departamento'	=> $departamento,
				'direccion'			=> $direccion,
				'localidad'			=> $localidad,
				'telefono'			=> $telefono,
				'email'					=> $email,
				'matriculado'		=> $matriculado,
				'carta'					=> $carta,
				'cv'						=> $file_renamed
			);
			
			// Send mail
			// Mail::send('emails.welcome', $htmlData, function($message) use ($data)
			Mail::send('emails.rrhh', $htmlData, function($message) use ($data)
			{
				$message->from( $data['email'] );
				$message->to( $data['to'] , '+VIDA' )->subject( $data['subject'] );
			});
			
			// dd(Mail::failures());
			if( count(Mail::failures()) == 0 ) {
				return view('mensajes.rrhh');
			}else{
				return view('mensajes.rrhh');
			}
			
		}
		
		
	}

}
