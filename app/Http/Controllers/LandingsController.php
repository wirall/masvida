<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateLandingsRequest;
use App\Http\Requests\UpdateLandingsRequest;
use App\Repositories\LandingsRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use InfyOm\Generator\Controller\AppBaseController;
use File;
use App\Models\Landings;


class LandingsController extends AppBaseController
{
    /** @var  LandingsRepository */
    private $landingsRepository;

    public function __construct(LandingsRepository $landingsRepo)
    {
        $this->landingsRepository = $landingsRepo;
    }


    /**
     * Display the specified Landings.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($url)
    {
        $landings = Landings::where('url' , '=', $url)->first();

        $inicio = strtotime($landings->inicio); 
        $fin = strtotime($landings->fin); 
        $fecha = strtotime(date("Y-m-d")); 
         
        if ($fecha >= $inicio && $fecha <= $fin ) {
            $activo = true;
        } else {
            $activo = false;
        }

        if (empty($landings)) {
            Flash::error('Landings not found');
            return redirect(route('landings.index'));
        }

        return view('partials.landing')->with('landings', $landings)->with('activo', $activo);
    }




}
