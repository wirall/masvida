<?php 

namespace App\Http\Controllers;
use App\Http\Requests\ServiciosFormRequest;
use App\Servicios;
use Request;
use Mail;

class ServiciosController extends Controller {
	
	public function store(ServiciosFormRequest $request){
		
		$empresa = 			Request::input('empresa');
		$cargo = 				Request::input('cargo');
		$nombre = 			Request::input('nombre');
		$apellido = 		Request::input('apellido');
		$telefono = 		Request::input('telefono');
		$email = 				Request::input('email');
		$localidad = 		Request::input('localidad');
		$cantidad = 		Request::input('cantidad');
		$rubro = 				Request::input('rubro');
		$comentarios = 	Request::input('comentarios');
		$info = 				Request::input('info');

		$servicios = new Servicios;
		$servicios->empresa = 		$empresa;
		$servicios->cargo = 			$cargo;
		$servicios->nombre = 			$nombre;
		$servicios->apellido = 		$apellido;
		$servicios->telefono = 		$telefono;
		$servicios->email = 			$email;
		$servicios->localidad = 	$localidad;
		$servicios->cantidad = 		$cantidad;
		$servicios->rubro = 			$rubro;
		$servicios->comentarios = $comentarios;
		$servicios->info = 				$info;

		if( $servicios->save() ){

			// Data message
			$data = array(
				'email'		=> $email,
				'subject'	=> 'Contacto desde el sitio +Vida (Servicios)',
				'to'			=> 'contacto@masvida.com.ar'
			);

			// the data that will be passed into the mail view blade template
			$htmlData = array(
				'empresa'				=> $empresa,
				'cargo'					=> $cargo,
				'nombre'				=> $nombre,
				'apellido'			=> $apellido,
				'telefono'			=> $telefono,
				'email'					=> $email,
				'localidad'			=> $localidad,
				'cantidad'			=> $cantidad,
				'rubro'					=> $rubro,
				'comentarios'		=> $comentarios
			);
			
			// Send mail
			Mail::send('emails.servicios', $htmlData, function($message) use ($data)
			{
				$message->from( $data['email'] );
				$message->to( $data['to'] , '+VIDA' )->subject( $data['subject'] );
			});
			
			// dd(Mail::failures());
			if( count(Mail::failures()) == 0 ) {
				return view('mensajes.servicios');
			}else{
				return view('mensajes.servicios');
			}



		}

	}
}
