<?php namespace App\Http\Controllers;

use App\Http\Requests\CvFormRequest;

class TestController extends Controller {

	public function filosofia(ServiciosFormRequest $request)
	{
		return view('partials.test');
	}

	public function rrhh(CvFormRequest $request)
	{
		return view('partials.test');
	}

}
