<?php namespace App\Http\Controllers;
use Request;
use App\Http\Requests\FamiliaFormRequest;
use App\Familia;
use Mail;

class FamiliaController extends Controller {

	public function index()
	{
		return view('partials.familia');
	}

	public function store(FamiliaFormRequest $request){
		
		$nombre 			= Request::input('nombre');
		$apellido 		= Request::input('apellido');
		$telefono 		= Request::input('telefono');
		$email 				= Request::input('email');
		$localidad 		= Request::input('localidad');
		$cantidad 		= Request::input('cantidad');
		$comentarios 	= Request::input('comentarios');
		$info 				= Request::input('info');

		$familia = new Familia;
		$familia->nombre 			= $nombre;
		$familia->apellido		= $apellido;
		$familia->telefono 		= $telefono;
		$familia->email				= $email;
		$familia->localidad		= $localidad;
		$familia->cantidad		= $cantidad;
		$familia->comentarios	= $comentarios;
		$familia->info				= $info;

		if( $familia->save() ){
			
			// Data message
			$data = array(
				'email'		=> $email,
				'subject'	=> 'Contacto desde el sitio +Vida (Familia)',
				'to'			=> 'contacto@masvida.com.ar',
				//'to'			=> 'giannina@wirallinteractive.com.ar'
			);

			// the data that will be passed into the mail view blade template
			$htmlData = array(
				'nombre'			=> $nombre,
				'apellido'		=> $apellido,
				'telefono'		=> $telefono,
				'email'				=> $email,
				'localidad'		=> $localidad,
				'cantidad'		=> $cantidad,
				'comentarios'	=> $comentarios,
				'info'				=> $info
			);
			
			// Send mail
			// Mail::send('emails.welcome', $htmlData, function($message) use ($data)
			Mail::send('emails.familia', $htmlData, function($message) use ($data)
			{
				$message->from( $data['email'] );
				$message->to( $data['to'] , '+VIDA' )->subject( $data['subject'] );
			});
			
			// dd(Mail::failures());
			if( count(Mail::failures()) == 0 ) {
				return view('mensajes.familia');
			}else{
				return view('mensajes.familia');
			}

		}
	}

}
