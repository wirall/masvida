<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;
class SitemapController extends Controller {

	public function sitemap()
	{
	    
	    return Response::view('sitemap.sitemap')->header('Content-Type', 'application/xml');
	}

}
