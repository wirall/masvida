<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Auth
Route::get('/auth/login', 'Auth\AuthController@getLogin');
Route::post('/auth/login', 'Auth\AuthController@postLogin');
Route::get('/auth/logout', 'Auth\AuthController@getLogout');
Route::get('/register', 'Auth\AuthController@getRegister');
Route::post('/register', 'Auth\AuthController@postRegister');


// Authentication routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


/*// Roles
Route::get('/', [
  'middleware' => ['auth', 'roles'],
  'uses' => 'AppController@index',
  'roles' => ['user']
]);
Route::get('/home', [
  'middleware' => ['auth', 'roles'],
  'uses' => 'AppController@index',
  'roles' => ['user']
]);*/

// BACKEND - routes
Route::group([
  'prefix' => 'admin',
  'middleware' => ['auth', 'roles'],
  'roles' => ['administrator','Root','Manager','Company Manager']
  ], 
  function () { 
    Route::get('/', 'AppController@backend');
});

Route::group([
  'middleware' => ['auth', 'roles'],
  'roles' => ['administrator','Root', 'Company Manager']
  ], 
  function(){
    Route::resource('noticias', 'Backend\NoticiasController');
    Route::resource('formsLandings', 'Backend\FormsLandingsController', ['except' => ['store']]);
    Route::resource('landings', 'Backend\LandingsController');
});


//FRONTEND - Vistas
Route::get('/', 'IndexController@index');
Route::get('filosofia', 'FilosofiaController@index');
Route::get('internacion-domiciliaria', 'InternacionDomiciliariaController@index');
Route::get('rrhh', 'RrhhController@index');
Route::get('familia', 'FamiliaController@index');
Route::get('empresas', 'EmpresasController@index');
Route::get('serviciosempresas/areaprotegida', 'ProtegidaController@index');
Route::get('serviciosempresas/areacardio', 'AreacardioController@index');
Route::get('serviciosempresas/medicinalaboral', 'MedicinalaboralController@index');
Route::get('serviciosempresas/eventos', 'EventosController@index');
Route::get('serviciosempresas/planes', 'PlanesController@index');
Route::get('serviciosempresas/serviciomedico', 'ServiciomedicoController@index');
Route::get('serviciosempresas/cursos', 'CursosController@index');
Route::get('serviciosempresas/cursos/mail', 'CursosController@index');
Route::get('serviciosempresas/medicinalaboral/mail', 'MedicinalaboralController@index');
Route::get('/novedades', 'NoticiasController@index');
Route::get('/novedad/{slug}', 'NoticiasController@show');
Route::get('/novedades/{order}','NoticiasController@showOrder');
//landing
Route::get('form/{slug}', 'LandingsController@show');
//form landing
Route::post('formsLandings', [
  'uses' => 'Backend\FormsLandingsController@store',
  'as' => 'formsLandings.store'
]);


//FRONTEND - Posts
Route::post('postrrhh','RrhhController@store');
Route::post('postfamilia','FamiliaController@store');
Route::post('serviciosempresas/areaprotegidapost','ServiciosController@store');
Route::post('serviciosempresas/areacardiopost','ServiciosController@store');
Route::post('serviciosempresas/medicinalaboralpost','ServiciosController@store');
Route::post('serviciosempresas/eventospost','ServiciosController@store');
Route::post('serviciosempresas/planespost','ServiciosController@store');
Route::post('serviciosempresas/serviciomedicopost','ServiciosController@store');
Route::post('serviciosempresas/cursospost','ServiciosController@store');
Route::post('filosofiapost','ServiciosController@store');
Route::post('internacion-domiciliariapost','ServiciosController@store');
Route::post('serviciosempresas/cursos/mailpost','ServiciosController@store');
Route::post('serviciosempresas/medicinalaboral/mailpost', 'ServiciosController@store');


//Sitemap 
Route::get('sitemap.xml', 'SitemapController@sitemap');
//Tester
/*Route::get('email', function()
{
    return view('emails.servicios');
});*/



/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
        require config('infyom.laravel_generator.path.api_routes');
    });
});




