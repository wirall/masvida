<?php

namespace App\Repositories;

use App\Models\Noticias;
use InfyOm\Generator\Common\BaseRepository;

class NoticiasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'titulo',
        'descripcion',
        'image',
        'image_thumb',
        'url'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Noticias::class;
    }
}
