<?php

namespace App\Repositories;

use App\Models\FormsLandings;
use InfyOm\Generator\Common\BaseRepository;

class FormsLandingsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'apellido',
        'telefono',
        'email',
        'id_landing'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FormsLandings::class;
    }
}
