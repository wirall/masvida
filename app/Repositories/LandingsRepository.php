<?php

namespace App\Repositories;

use App\Models\Landings;
use InfyOm\Generator\Common\BaseRepository;

class LandingsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'texto',
        'texto_caducidad',
        'imagen',
        'inicio',
        'fin',
        'url',
        'servicios'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Landings::class;
    }
}
