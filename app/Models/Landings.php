<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Landings",
 *      required={titulo, texto, texto_caducidad, imagen, inicio, fin, url, servicios, activo},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="titulo",
 *          description="titulo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="imagen",
 *          description="imagen",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="inicio",
 *          description="inicio",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="fin",
 *          description="fin",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="url",
 *          description="url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="servicios",
 *          description="servicios",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="activo",
 *          description="activo",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Landings extends Model
{
    use SoftDeletes;

    public $table = 'landings';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'texto',
        'texto_caducidad',
        'imagen',
        'inicio',
        'fin',
        'url',
        'servicios'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'titulo' => 'string',
        'imagen' => 'string',
        'inicio' => 'date',
        'fin' => 'date',
        'url' => 'string',
        'servicios' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required',
        'texto' => 'required',
        'texto_caducidad' => 'required',
        'imagen' => 'required',
        'inicio' => 'required',
        'fin' => 'required',
        'url' => 'required|unique:landings',
        'servicios' => 'required'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules_update = [
        'titulo' => 'required',
        'texto' => 'required',
        'texto_caducidad' => 'required',
        'inicio' => 'required',
        'fin' => 'required',
        'url' => 'required',
        'servicios' => 'required'
    ];
}
