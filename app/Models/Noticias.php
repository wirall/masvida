<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Noticias",
 *      required={titulo, titulo, descripcion, image, url},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="titulo",
 *          description="titulo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="titulo",
 *          description="titulo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="image",
 *          description="image",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="url",
 *          description="url",
 *          type="string"
 *      )
 * )
 */
class Noticias extends Model
{
    use SoftDeletes;

    public $table = 'noticias';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'sub_titulo',
        'descripcion',
        'image',
        'image_thumb',
        'url'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'titulo' => 'string',
        'sub_titulo' => 'string',
        'descripcion' => 'string',
        'image' => 'string',
        'image_thumb' => 'string',
        'url' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required',
        'sub_titulo' => 'required',
        'descripcion' => 'required',
        'image_thumb' => 'required',
        'image' => 'required',
        'url' => 'required|unique:noticias'
    ];

    public static $rules_update = [
        'titulo' => 'required',
        'sub_titulo' => 'required',
        'descripcion' => 'required',
        'url' => 'required'
    ];
}
