<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
      DB::table('users')->truncate();

	    $values = array(

	      array(
          'email' => 'admin@admin.com',
          'password' => bcrypt('w1r4ll'),
	        'role_id' => 1,
	      ),
        array(
          'email' => 'admin@masvida.com.ar',
          'password' => bcrypt('hydp#N3ty2#J'),
          'role_id' => 1,
        )

	    );

	    DB::table('users')->insert( $values );

    }
}