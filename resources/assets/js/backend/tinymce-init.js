$( document ).ready(function() {

  // Text Editor
  // !!!! IMPORTANTE !!!!!!!!!!
  // ESTE PLIGIN NO FUNCIONA CON EL GULP WATCH. CORRER EL COMANDO GULP CADA VEZ QUE SE HAGA UN CAMBIO
  tinymce.init({
    selector : ".tinymce-init",
    //menubar: '',
    menubar: false,
    statusbar: false,
    plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste", "textcolor"],
    toolbar: "insertfile undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor fontselect",
    font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
    setup : function(ed) {
      ed.on('change', function(e) {
         $('input[name=descripcion_hidden]').val( ed.getContent() );
      }),
      ed.on('init', function(e) {
         $('input[name=descripcion_hidden]').val( ed.getContent() );
      })
    }
  });
 

});
