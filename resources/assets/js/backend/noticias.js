$( document ).ready(function() {

	$("#titulo").bind("keydown", function(e){
		var titulo = $("#titulo").val();
		var slug = convertToSlug(titulo);
		$('#url').val(slug);
		$('#url').text(slug);
	});

	$("#titulo").bind("change", function(e){
		var titulo = $("#titulo").val();
		var slug = convertToSlug(titulo);
		$('#url').val(slug);
		$('#url').text(slug);
	});

	$("#url").bind("keypress", function(e){
		console.log('keypress URL');
	  	var titulo = $("#url").val();
	  	var slug = convertToSlug(titulo);
	  	$(this).val(slug);
	});

	function convertToSlug(Text)
	{
	    return Text
	        .toLowerCase()
	        .replace(/ /g,'-')
	        .replace(/[^\w-]+/g,'')
	        ;
	}

});