<div class="col-sm-12">
    <ul class="alert alert-danger date-error" style="list-style-type: none; display: none">
        <li>La fecha de fin no puede ser menor o igual a la fecha de inicio</li>
    </ul>
</div>

<!-- Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inicio', 'Inicio:') !!}
    {!! Form::date('inicio', null, ['class' => 'form-control', 'id' => 'date-1']) !!}
</div>

<!-- Fin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fin', 'Fin:') !!}
    {!! Form::date('fin', null, ['class' => 'form-control', 'id' => 'date-2']) !!}
</div>

<!-- Titulo Field -->
<div class="form-group col-sm-12">
    {!! Form::label('titulo', 'Titulo:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control', 'id' => 'titulo']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-12">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, ['class' => 'form-control', 'id' => 'url']) !!}
</div>

<!-- Texto Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('texto', 'Texto:') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control tinymce-init']) !!}
</div>

<!-- Texto Caducidad Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('texto_caducidad', 'Texto Caducidad:') !!}
    {!! Form::textarea('texto_caducidad', 'Esta página ya ha caducado.', ['class' => 'form-control']) !!}
</div>

<!-- Imagen Field -->
<div class="form-group col-sm-12">
    {!! Form::label('imagen', 'Imagen:') !!} <small>Tamaño recomendado: 1920px x 600px</small>
    {!! Form::file('imagen', ['class' => 'filestyle', 'data-buttonText' => 'Buscar']) !!}
</div>
<div class="clearfix"></div>

<!-- Servicios Field -->
<div class="form-group col-sm-12">
    {!! Form::label('servicios', 'Servicios:') !!}
    <select class='form-control' id="servicios" name="servicios">
    @foreach($servicios_list as $servicios)
        <option vlaue="{{ $servicios }}">{{ $servicios }}</option>
    @endforeach
    </select>
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('landings.index') !!}" class="btn btn-default">Cancelar</a>
</div>
