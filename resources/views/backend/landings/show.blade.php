@extends('backend.main')

@section('content')
    @include('landings.show_fields')

    <div class="form-group">
           <a href="{!! route('landings.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
