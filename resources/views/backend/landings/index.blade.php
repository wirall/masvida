@extends('backend.main')

@section('content')

	<div class="container">

	    <h1 class="pull-left"><i class="fa fa-file-image-o" aria-hidden="true"></i> Landings</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('landings.create') !!}">
            <span class="glyphicon glyphicon-plus"></span> 
            Agregar
        </a>
        <hr>

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        @include('backend.landings.table')

    </div>
        
@endsection