<div class="col-sm-12">
    <ul class="alert alert-danger date-error" style="list-style-type: none; display: none">
        <li>La fecha de fin no puede ser menor o igual a la fecha de inicio</li>
    </ul>
</div>

<!-- Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inicio', 'Inicio:') !!}
    {!! Form::date('inicio', date_format($landings->inicio, 'Y-m-d') , ['class' => 'form-control', 'id' => 'date-1']) !!}
</div>

<!-- Fin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fin', 'Fin:') !!}
    {!! Form::date('fin', date_format($landings->fin, 'Y-m-d') , ['class' => 'form-control', 'id' => 'date-2']) !!}
    
</div>

<!-- Titulo Field -->
<div class="form-group col-sm-12">
    {!! Form::label('titulo', 'Titulo:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-12">
    {!! Form::label('url', 'Url:') !!} <small> (La url no puede ser editada para evitar inconsistencias en SEO.) </small>
    {!! Form::text('url', null, ['class' => 'form-control', 'disabled' => 'true']) !!}
    <input id="token" type="hidden" name="url" value="{{ $landings->url}}">
</div>

<!-- Texto Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('texto', 'Texto:') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control tinymce-init']) !!}
</div>

<!-- Texto Caducidad Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('texto_caducidad', 'Texto Caducidad:') !!}
    {!! Form::textarea('texto_caducidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->   
<div class="col-sm-12">
    {!! Form::label('thumb', 'Imagen:') !!} <small>Tamaño recomendado: 1920px x 600px</small>
    </div>
<div class="col-sm-2">
    {!! Html::image('uploads/landings/'.$landings->imagen, 'Imagen', ['class' => 'img-responsive center-block']) !!}
</div>
<div class="col-sm-10">
    {!! Form::file('imagen', ['class' => 'filestyle', 'data-buttonText' => 'Buscar', 'data-placeholder' => $landings->imagen]) !!}
</div>

<div class="clearfix"></div>

<!-- Servicios Field -->
<div class="form-group col-sm-12">
    {!! Form::label('servicios', 'Servicios:') !!}
    <select class='form-control' id="servicios" name="servicios">
    @foreach($servicios_list as $servicios)
        <option vlaue="{{ $servicios }}" @if($servicios == $landings->servicios) selected @endif>{{ $servicios }}</option>
    @endforeach
    </select>
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('landings.index') !!}" class="btn btn-default">Cancelar</a>
</div>
