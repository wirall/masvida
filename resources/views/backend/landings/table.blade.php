<table class="table table-responsive" id="tabla-datatable">
    <thead>
        <th>Inicio</th>
        <th>Fin</th>
        <th class="hide-filter">Imagen</th>
        <th>Titulo</th>
        <th>Servicios</th>
        <th class="hide-filter"></th>
    </thead>
    <tbody>
    @foreach($landings as $landings)
        <tr>
            <td>{{ date_format($landings->inicio, 'd/m/y') }}</td>
            <td>{{ date_format($landings->fin, 'd/m/y') }}</td>
            <td><img src="{!! URL::asset('/uploads/landings/'.$landings->imagen) !!}" height="50" width="auto"></td>
            <td>{!! $landings->titulo !!}</td>
            <td>{!! $landings->servicios !!}</td>
            <td>
                {!! Form::open(['route' => ['landings.destroy', $landings->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="form/{!! $landings->url !!}" class='btn btn-default btn-xs' target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('landings.edit', [$landings->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Esta seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>