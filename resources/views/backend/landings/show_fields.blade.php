<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $landings->id !!}</p>
</div>

<!-- Titulo Field -->
<div class="form-group">
    {!! Form::label('titulo', 'Titulo:') !!}
    <p>{!! $landings->titulo !!}</p>
</div>

<!-- Texto Field -->
<div class="form-group">
    {!! Form::label('texto', 'Texto:') !!}
    <p>{!! $landings->texto !!}</p>
</div>

<!-- Texto Caducidad Field -->
<div class="form-group">
    {!! Form::label('texto_caducidad', 'Texto Caducidad:') !!}
    <p>{!! $landings->texto_caducidad !!}</p>
</div>

<!-- Imagen Field -->
<div class="form-group">
    {!! Form::label('imagen', 'Imagen:') !!}
    <p>{!! $landings->imagen !!}</p>
</div>

<!-- Inicio Field -->
<div class="form-group">
    {!! Form::label('inicio', 'Inicio:') !!}
    <p>{!! $landings->inicio !!}</p>
</div>

<!-- Fin Field -->
<div class="form-group">
    {!! Form::label('fin', 'Fin:') !!}
    <p>{!! $landings->fin !!}</p>
</div>

<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', 'Url:') !!}
    <p>{!! $landings->url !!}</p>
</div>

<!-- Servicios Field -->
<div class="form-group">
    {!! Form::label('servicios', 'Servicios:') !!}
    <p>{!! $landings->servicios !!}</p>
</div>

<!-- Activo Field -->
<div class="form-group">
    {!! Form::label('activo', 'Activo:') !!}
    <p>{!! $landings->activo !!}</p>
</div>

