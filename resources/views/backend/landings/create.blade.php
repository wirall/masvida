@extends('backend.main')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left"><i class="fa fa-file-image-o" aria-hidden="true"></i> Crear Landing</h1>
                <hr>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::open(['route' => 'landings.store', 'files' => true]) !!}

                @include('backend.landings.fields')

            {!! Form::close() !!}
        </div>


    </div>
@endsection

@section('viewscripts')
    <script src="{{ asset('js/backend/landings.js') }}"></script>
    <script src="{{ asset('js/backend/tinymce-init.js') }}"></script>
@endsection