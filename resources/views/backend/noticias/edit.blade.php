@extends('backend.main')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Editar Noticia</h1>
                <hr>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($noticias, ['route' => ['noticias.update', $noticias->id], 'method' => 'patch', 'files' => true]) !!}

            @include('backend.noticias.fields_edit')

            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('viewscripts')
    <script src="{{ asset('js/backend/tinymce-init.js') }}"></script>
@endsection