@extends('backend.main')

@section('content')

	<div class="container">

	    <h1 class="pull-left"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Noticias</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('noticias.create') !!}">
            <span class="glyphicon glyphicon-plus"></span> 
            Agregar
        </a>
        <hr>

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        @include('backend.noticias.table')
    </div>   
@endsection