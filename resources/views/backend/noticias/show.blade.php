@extends('backend.main')

@section('content')
    @include('backend.noticias.show_fields')

    <div class="form-group">
           <a href="{!! route('noticias.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
