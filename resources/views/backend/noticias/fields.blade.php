<!-- Titulo Field -->
<div class="form-group col-sm-12">
    {!! Form::label('titulo', 'Titulo:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control', 'id' => 'titulo']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-12">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, ['class' => 'form-control', 'id' => 'url']) !!}
</div>

<!-- Titulo Field -->
<div class="form-group col-sm-12">
    {!! Form::label('sub_titulo', 'Sub Titulo:') !!}
    {!! Form::text('sub_titulo', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control  tinymce-init']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image', 'Imagen Portada:') !!} <small>Tamaño recomendado: 1200px x 500px</small>
    {!! Form::file('image', ['class' => 'filestyle', 'data-buttonText' => 'Buscar']) !!}
</div>

<div class="col-sm-12">
    <br>
</div>

<div class="clearfix"></div>
<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image_thumb', 'Imagen Miniatura:') !!} <small>Tamaño recomendado: 500px x 500px</small>
    {!! Form::file('image_thumb', ['class' => 'filestyle', 'data-buttonText' => 'Buscar']) !!}
</div>
<div class="clearfix"></div>

<br>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('noticias.index') !!}" class="btn btn-default">Cancelar</a>
</div>
