<table class="table table-responsive" id="tabla-datatable">
    <thead>
        <th>Imagen Miniatura</th>
        <th>Imagen</th>
        <th>Titulo</th>
        <th>Sub Titulo</th>
        <th>Url</th>
        <th > </th>
    </thead>
    <tbody>
    @foreach($noticias as $noticias)
        <tr>
            <td><img src="{!! URL::asset('/uploads/noticias/'.$noticias->image_thumb) !!}" height="50" width="auto"></td>
            <td><img src="{!! URL::asset('/uploads/noticias/'.$noticias->image) !!}" height="50" width="auto"></td>
            <td>{!! $noticias->titulo !!}</td>
            <td>{!! $noticias->sub_titulo !!}</td>
            <td>{!! $noticias->url !!}</td>
            <td>
                {!! Form::open(['route' => ['noticias.destroy', $noticias->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="/novedad/{{$noticias->url}}" target="_blank" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('noticias.edit', [$noticias->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Esta seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>