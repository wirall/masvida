<div class="bs-callout bs-callout-danger" id="callout-badges-ie8-empty"> 
    <h4 class="text-uppercase" > Datos de la landing</h4> 
</div>
<div class="jumbotron detalle">
    <h4><strong>{!! $landing->titulo !!}</strong></h4>
    <h6><strong>SERVICIO:</strong> {!! $landing->servicios !!}</h6>
    <h6><strong>INICIO:</strong> {{ date_format($landing->inicio, 'd/m/y') }} | <strong>FIN:</strong> {{ date_format($landing->fin, 'd/m/y') }}</h6>
    <p><a href="/landing/{!! $landing->url !!}">(VER LANDING)</a></p>
</div>


<div class="bs-callout bs-callout-danger" id="callout-badges-ie8-empty"> 
    <h4 class="text-uppercase" > Datos del Email</h4> 
</div>
<ul class="list-group">
  <li type="button" class="list-group-item"><strong>Fecha:</strong> {{ date_format($formsLandings->created_at, 'd/m/y') }}</li>
  <li type="button" class="list-group-item"><strong>Nombre:</strong> {!! $formsLandings->nombre !!}</li>
  <li type="button" class="list-group-item"><strong>Apellido:</strong> {!! $formsLandings->apellido !!}</li>
  <li type="button" class="list-group-item"><strong>Teléfono:</strong> {!! $formsLandings->telefono !!}</li>
  <li type="button" class="list-group-item"><strong>Mensaje:</strong> {!! $formsLandings->mensaje !!}</li>
</ul>
