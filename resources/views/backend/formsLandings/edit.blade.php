@extends('backend.main')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Forms-Landings</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($formsLandings, ['route' => ['formsLandings.update', $formsLandings->id], 'method' => 'patch']) !!}

            @include('backend.formsLandings.fields')

            {!! Form::close() !!}
        </div>
@endsection