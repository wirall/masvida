@extends('backend.main')

@section('content')

	<div class="container">

	    <h1 class="pull-left"><i class="fa fa-envelope" aria-hidden="true"></i> Email </h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('formsLandings.index') !!}">
            Volver
        </a>
        <hr>

		<div class="col-xs-12">
	   		@include('backend.formsLandings.show_fields')
	    </div>

	</div>
@endsection
