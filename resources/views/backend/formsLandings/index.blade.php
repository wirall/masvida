@extends('backend.main')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Emails recibidos</h1>
                <hr>
            </div>
        </div>

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        @include('backend.formsLandings.table')

    </div>
        
@endsection