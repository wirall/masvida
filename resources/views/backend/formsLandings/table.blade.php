<table class="table table-responsive" id="tabla-datatable">
    <thead>
        <th>Fecha</th>
        <th>Email</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Telefono</th>
        <th>Landing</th>
        <th class="hide-filter"> </th>
    </thead>
    <tbody>
    @foreach($formsLandings as $formsLandings)
        <tr>
            <td> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $formsLandings->fechaFormsLandings)->format('d/m/y')}}</td>
            <td>{!! $formsLandings->email !!}</td>
            <td>{!! $formsLandings->nombre !!}</td>
            <td>{!! $formsLandings->apellido !!}</td>
            <td>{!! $formsLandings->telefono !!}</td>
            <td>{!! $formsLandings->tituloLanding !!}</td>
            <td>
                {!! Form::open(['route' => ['formsLandings.destroy', $formsLandings->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('formsLandings.show', [$formsLandings->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Esta seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>