<!DOCTYPE html>
<html lang="en">
      <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=9;IE=10;IE=Edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="Template">
        <meta name="author" content="Wirall Interactive">

        <title>@yield("title", "Mas Vida - Backend")</title>
        <!-- Favicon -->
        <link rel="icon" href="{{ asset('/favicon.png') }}" type="image/png" sizes="16x16">
        <!-- Vendors -->
        <link rel="stylesheet" href="{{ asset('/css/vendors.css') }}">
        <!-- Custom -->
        <link href="{{ asset('/css/backend/app.css') }}" rel="stylesheet">

    </head>

  <body class="Theme-Red">
        {{-- NAVBAR TABLET / MOBILE --}}
        <div class="navmenu navmenu-default navmenu-fixed-left offcanvas-sm" role="navigation">
          
          <img class="navmenu-brand" src="/css/img/logo.png" height="40" width="auto">

          <ul class="nav navmenu-nav">

                <li class="@if(Request::is ('admin')) active @endif">
                  <a href="/admin" class=""><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a>
                </li>

                <div class="accordion" id="accordion2">
            

                  <li class="accordion-group">
                    <div class="accordion-heading @if(
                                    Request::is ('noticias') || Request::is ('noticias/*')
                                    ) active @endif" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                      <a class="accordion-toggle">
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i> Noticias <small class="fa fa-caret-down" aria-hidden="true"></small>
                      </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse in @if(
                                    Request::is ('noticias') || Request::is ('noticias/*')
                                    )in @endif">
                      <div class="accordion-inner">
                        <ul>
                          <li class="@if(Request::is ('noticias') ) active @endif">
                            <a href="/noticias" class=""><i class="fa fa-list" aria-hidden="true"></i> Lista de Noticias</a>
                          </li>
                          <li class="@if(Request::is ('noticias/create')) active @endif">
                            <a href="/noticias/create" class=""><i class="glyphicon glyphicon-plus"></i> Agregar Noticia</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </li>

                  <li class="accordion-group">
                    <div class="accordion-heading @if(
                                    Request::is ('landings') || Request::is ('landings/*')
                                    ) active @endif" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                      <a class="accordion-toggle">
                        <i class="fa fa-file-image-o" aria-hidden="true"></i> Landings <small class="fa fa-caret-down" aria-hidden="true"></small>
                      </a>
                    </div>
                    <div id="collapseTwo" class="accordion-body collapse in @if(
                                    Request::is ('landings') || Request::is ('landings/*')
                                    )in @endif">
                      <div class="accordion-inner">
                        <ul>
                          <li class="@if(Request::is ('landings') ) active @endif">
                            <a href="/landings" class=""><i class="fa fa-list" aria-hidden="true"></i> Lista de Landings</a>
                          </li>
                          <li class="@if(Request::is ('landings/create')) active @endif">
                            <a href="/landings/create" class=""><i class="glyphicon glyphicon-plus"></i> Agregar Landing</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </li>

                </div>

                <li class="@if(Request::is ('formsLandings') || Request::is ('formsLandings/*')) active @endif">
                  <a href="/formsLandings" class=""><i class="fa fa-envelope" aria-hidden="true"></i> Emails Landings</a>
                </li>

          </ul>
        </div>


        {{-- NAVBAR DESKTOP --}}
        <div class="navbar navbar-default navbar-fixed-top">

          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle pull-left" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <img class="navmenu-brand" src="/css/img/logo.png" height="40" width="auto">
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             
              <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    @if ( Auth::check() )
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                      <i class="glyphicon glyphicon-user icon-user"></i> Mi Cuenta</a>
                    <ul class="dropdown-menu">
                    <li class="text-right"><a href="/auth/logout">Cerrar sesión <i class="fa fa-sign-out"></i></a></li>
                    @endif
                  </ul>
                </li>
              </ul>
            
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </div>
    
    @yield("content")

    <!-- Scripts -->
    <script src="{{ asset('js/vendors.js') }}"></script>
    <script src="{{ asset('js/backend/app.js') }}"></script>
    @yield("viewscripts")

  </body>


</html>
