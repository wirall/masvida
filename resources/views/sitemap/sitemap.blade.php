
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<!--
 created with Free Online Sitemap Generator www.xml-sitemaps.com 
-->
<url>
	<loc>http://masvida.com.ar/</loc>
</url>
<url>
	<loc>http://masvida.com.ar/familia</loc>
</url>
<url>
	<loc>http://masvida.com.ar/filosofia</loc>
</url>
<url>
	<loc>http://masvida.com.ar/rrhh</loc>
</url>
<url>
	<loc>http://masvida.com.ar/serviciosempresas/areaprotegida</loc>
</url>
<url>
	<loc>http://masvida.com.ar/serviciosempresas/areacardio</loc>
</url>
<url>
	<loc>http://masvida.com.ar/serviciosempresas/medicinalaboral</loc>
</url>
<url>
	<loc>http://masvida.com.ar/serviciosempresas/eventos</loc>
</url>
<url>
	<loc>http://masvida.com.ar/serviciosempresas/planes</loc>
</url>
<url>
	<loc>http://masvida.com.ar/serviciosempresas/serviciomedico</loc>
</url>
<url>
	<loc>http://masvida.com.ar/serviciosempresas/cursos</loc>
</url>
<url>
	<loc>http://masvida.com.ar/pdf/cartilla-mas-vida-2014.pdf</loc>
	<lastmod>2015-06-09T17:28:44+00:00</lastmod>
</url>
<changefreq>always</changefreq>
</urlset>
