@extends('main')
@section('title', '+VIDA - Noticias')
@section('content')



<div class="novedades col-xs-12">

	<div class="detalle-noticia col-xs-12 col-sm-12 col-md-8">

		<img src="{!! URL::asset('/uploads/noticias/'.$noticia->image) !!}">
						
	 	<h1 class="text-uppercase">{!! $noticia->titulo !!}</h1>
		<h4>{!! $noticia->sub_titulo !!}</h4>
		<div>{!! $noticia->descripcion !!}</div>

	</div>

	<div class="sidebar col-xs-12 col-sm-12 col-md-4">
		<h4 class="text-uppercase">OTRAS NOTICIAS</h4>

		@if(count($noticias) > 0)
			@foreach($noticias as $not)
			<a href="/novedad/{!! $not->url !!}">
				<div class="item">
					<div class="img col-xs-4" style="background-image: url({!! URL::asset('/uploads/noticias/'.$not->image_thumb) !!})">
					</div>	
					<div class="col-xs-8">
						<h5 class="text-uppercase">{!! $not->titulo !!}</h5>
				        <h6>{!! $not->sub_titulo !!}</h6>
				        <div class="descripcion">{!! str_limit($not->descripcion, 100) !!}</div>
					</div>			
				</div>
			</a>		
	 		@endforeach
	 	@else
	 		No se encontraron noticias.
 		@endif
	</div>

</div>


@endsection


