@extends('main')
@section('title', '+VIDA - Filosofía')
@section('content')
<div class="container-fluid">
	<div class="row">
		
		<div id="carousel-filosofia" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-home-03.png" alt="FILOSOFÍA. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner">
							<p>FILOSOFÍA</p>
							<p><img src="/css/img/copy-empresas.png" alt="FILOSOFÍA. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							{{-- <a class="btn btn-primary" href="#form">CONTRATAR <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a> --}}
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey">
			<div class="container text-center from-bottom">
				<h2>IDENTIDAD, BUENA FE, COLABORACIÓN Y EXCELENCIA</h2>

				<p>En <strong class="red">+VIDA</strong> trabajamos y nos comprometemos todos los días con nuestra máxima meta: ayudar a proteger la vida de nuestros clientes. <br /> Somos una empresa joven formada por profesionales de trayectoria y experiencia, con <strong class="red">más de 150.000 atenciones al año</strong>, móviles equipados con tecnología de última generación y una gran logística operativa que tiene como finalidad salvar vidas.</p>
			</div>
		</div>
		<div class="intro fade-in">
			<div class="container text-center from-bottom">
				<h2>NUESTRA MISIÓN</h2>
				<p>Ser una <strong class="red">organización médica con una alta performance</strong>, sustentable y con la mayor satisfacción de los clientes. Nuestro éxito depende de la <strong>capacidad de liderar de forma ética y responsable</strong>. Nuestro <strong>Código de Ética</strong> marca la conducta que se espera de todos nuestros colaboradores y también presenta la firme convicción acerca de cuáles son las mejores prácticas de mercado.</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-info text-center from-top">
			<div class="container">
				<h2>NUESTROS PRINCIPIOS</h2>
				<div class="row">
					<div class="col-sm-6 col-md-3">
						<div class="thumbnail from-top">
							<img src="css/img/icon-identidad.png" alt="PRINCIPIO DE IDENTIDAD">
							<div class="caption-filosofia">
								<h5>PRINCIPIO DE IDENTIDAD</h5>
								<p>Brindamos servicios con elevados estándares de ética, superando con flexibilidad las situaciones límites y respetando la sustentabilidad.</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="thumbnail from-top">
							<img src="css/img/icon-fe.png" alt="PRINCIPIO DE BUENA FE">
							<div class="caption-filosofia">
								<h5>PRINCIPIO DE BUENA FE</h5>
								<p>Actuamos con transparencia e idoneidad y asumimos la responsabilidad de nuestros actos y elecciones.</p>
							</div>
						</div>
					</div>
					<div class="clearfix visible-sm-block"></div>
					<div class="col-sm-6 col-md-3">
						<div class="thumbnail from-top">
							<img src="css/img/icon-colaboracion.png" alt="PRINCIPIO DE COLABORACIÓN">
							<div class="caption-filosofia">
								<h5>PRINCIPIO DE COLABORACIÓN</h5>
								<p>Estamos abiertos al diálogo e interactuamos con nuestro público con el fin de compartir acciones y objetivos que lleven al bien común.</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="thumbnail from-top">
							<img src="css/img/icon-excelencia.png" alt="PRINCIPIO DE EXCELENCIA">
							<div class="caption-filosofia">
								<h5>PRINCIPIO DE EXCELENCIA</h5>
								<p>Cultivamos ambientes que propicien la realización de un trabajo de alta calidad, relevante para quien lo ejecuta, para la institución y para la sociedad.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro">
			<div class="container text-center from-bottom">
				<h3>NUESTRA HISTORIA</h3>
				<p>Somos un grupo multidisciplinario de expertos en <strong class="red">Servicios Médicos Domiciliarios</strong>, que constituimos una organización con  la que iniciamos nuestra actividad como empresa de Emergencias Médicas. Día tras día, hemos ido sumando más prestaciones, mayor área geográfica y más expertise:</p>

				<ul class="row text-left">
					<li class="col-sm-6"><strong>Internación Domiciliaria</strong>, entendiendo que el aumento de la expectativa de vida genera una población (cuya cantidad crece rápidamente), que requiere servicios de salud sin necesidad de institucionalizarse.</li>
					<li class="col-sm-6"><strong>Cuerpo de docencia</strong>, con programas de formación  interna (Médicos, Paramédicos y Enfermeros), programa para clientes (Industrias, Comercios, Colegios, Entidades deportivas) y Comunidad.</li>
					<li class="col-sm-6"><strong>Medicina Laboral</strong>, para acompañar a nuestras empresas cliente, con soluciones a sus necesidades.</li>
				</ul>

				<div class="row">
					<div class="col-sm-6 from-left">
						<div class="well">
							<p>NUESTRA MODALIDAD DE TRABAJO ES ARTICULAR REDES DE PRESTADORES LOCALES, PARA PODER BRINDAR SOLUCIONES AJUSTADAS A CADA NECESIDAD, EN LOS LUGARES QUE NOS REQUIERAN, HACIENDO EL SISTEMA COMPLETO MÁS EFICIENTE.</p>
						</div>
					</div>
					<div class="col-sm-6 from-right">
						<div class="well">
							<p>LAS SOLUCIONES LAS GENERAMOS CON UN EQUIPO DE CONDUCCIÓN MÉDICO (ACTUALMENTE INTEGRADO POR 7 PROFESIONALES DE LA MEDICINA) ESPECIALIZADO EN LA GESTIÓN DE LA SALUD PÚBLICA Y PRIVADA, JUNTO A UN EQUIPO CON AÑOS DE EXPERIENCIA EN LA SALUD AMBULATORIA.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.servicios')
			</div>
		</div>
	</div>
</div>
</div>
@endsection
