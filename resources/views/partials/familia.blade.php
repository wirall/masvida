@extends('main')
@section('title', '+VIDA - Familia')
@section('content')
<div class="container-fluid">
	<div class="row">
		
		<div id="carousel-familia" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-familia-01.png" alt="FAMILIA. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner">
							<p>FAMILIA</p>
							<p><img src="/css/img/copy-empresas.png" alt="FAMILIA. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							<a class="btn btn-primary" href="#form">COTIZAR PLAN <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>

	</div>

	<div class="row">
		<div class="intro">
			<div class="container">
				<h3 class="text-center">FAMILIA</h3>
				
				<div class="row from-bottom">
					<div class="col-sm-6">
						<p>Al igual que vos, nosotros en <strong class="red">+VIDA</strong> creemos que tu familia tiene que tener un cuidado especial, de alta calidad. Por eso diseñamos planes que se adaptan a tus necesidades, con profesionales capacitados y una amplia flota de unidades móviles de última tecnología..</p>
						<p>En <strong class="red">+VIDA</strong> tu familia encuentra más que un servicio de emergencias y cuidados médicos, encuentra contención. Nuestros profesionales están entrenados, no sólo para actuar con rapidez y eficiencia, sino también para establecer con calidez una atención de alta calidad médica.</p>
					</div>
					<div class="col-sm-6">
						<div class="table-responsive">
							<table class="table table-bordered table-striped">
								<tr>
									<th class="text-center" width="44%">PLANES</th>
									<th class="text-center" width="28%">MV1</th>
									<th class="text-center" width="28%">MV+</th>
								</tr>
								<tr>
									<td>EMERGENCIAS MÉDICAS</td>
									<td class="text-center"><div class="diamond"></div></td>
									<td class="text-center"><div class="diamond"></div></td>
								</tr>
								<tr>
									<td>URGENCIAS MÉDICAS</td>
									<td class="text-center"><div class="diamond"></div></td>
									<td class="text-center"><div class="diamond"></div></td>
								</tr>
								<tr>
									<td>MÉDICO A DOMICILIO</td>
									<td class="text-center">(COPAGO)</td>
									<td class="text-center"><div class="diamond"></div></td>
								</tr>
								<tr>
									<td>COBERTURA NACIONAL</td>
									<td class="text-center"><div class="diamond"></div></td>
									<td class="text-center"><div class="diamond"></div></td>
								</tr>
								<tr>
									<td>TRASLADOS PROGRAMADOS</td>
									<td class="text-center">(TP)</td>
									<td class="text-center">(TP)</div></td>
								</tr>
								<tr>
									<td>HOGAR PROTEGIDO</td>
									<td class="text-center">-</td>
									<td class="text-center"><div class="diamond"></div></td>
								</tr>
							</table>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
    
    <div class="row">
        <div class="container">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-10" style="text-align:center">
                <img src="css/img/nuevos-planes.png" alt="Familia" class="img-responsive">
            </div>
            <div class="col-sm-1">
            </div>
        </div>
    </div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.familia')
			</div>
		</div>
	</div>
</div>
</div>
@endsection
