@extends('main')

@section('head-scripts')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        
        function recaptcha_callback(){
          $('#recaptcha-response').val(grecaptcha.getResponse());
          $('#submitButton').removeAttr('disabled');
        }

    </script>
@endsection

@section('title', '+VIDA - Landing')

<?php 
    DEFINE('RECAPTCHA_KEY', '6LcAXkMUAAAAAOG_OYi__q6d7QLIRrD6hLH9-Pb1');
?>

@section('content')
<style>
    body {
        margin-bottom: 0px !important;
    }
    .footer {
        position: inherit !important;
        clear: both;
    }
</style>
    <div class="landing">
        
        @if ($activo == true)

            <div class="portada col-sm-12">
                <div class="row">
                    <div class="img" style="background-image:url('/uploads/landings/{!! $landings->imagen !!}')"></div>
                </div>
            </div>
            
            <!-- texto -->
            <div class="block-white col-sm-12">
                <div class="col-xs-12 col-sm-offset-0 col-sm-6 col-sm-offset-3">
                    <h1 class="text-center text-uppercase ">{!! $landings->titulo !!}</h1>
                    <div class="text-justify">{!! $landings->texto !!}</div>
                </div>
            </div>
            <!-- texto -->
            
            <!-- form -->
            <div class="block-gris-claro form col-sm-12">
                <div class="col-xs-12 col-sm-offset-0 col-sm-6 col-sm-offset-3">
                    <div class="row">
                        <h4 class="text-center text-uppercase ">FORMULARIO</h4>
                        @include('flash::message')
                        @include('core-templates::common.errors')
                        {!! Form::open(['route' => 'formsLandings.store']) !!}

                            <!-- Nombre Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('nombre', 'Nombre:') !!}
                                {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
                            </div>
                            <!-- Apellido Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('apellido', 'Apellido:') !!}
                                {!! Form::text('apellido', null, ['class' => 'form-control']) !!}
                            </div>
                            <!-- Telefono Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('telefono', 'Telefono:') !!}
                                {!! Form::number('telefono', null, ['class' => 'form-control']) !!}
                            </div>
                            <!-- email Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('email', 'Email:') !!}
                                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                            </div>
                            <!-- Id Landing Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::hidden('id_landing', $landings->id, ['class' => 'form-control']) !!}
                            </div>

                            </div>
                            <!-- Submit Field -->
		                    <div class="row">
                                <div class="col-sm-6" style="margin-bottom:20px">
                                    <div id="g-recaptcha" class="g-recaptcha" data-sitekey="{{ RECAPTCHA_KEY }}" data-callback="recaptcha_callback" data-expired-callback="recaptcha_expired_callback"></div>
                                    {!! Form::hidden('recaptcha-response', null, ['id' => 'recaptcha-response']) !!}                
                                </div>  
			                    <div class="col-sm-6 text-right">
				                    <div class="form-group">
			    	                    {!! Form::submit('ENVIAR', array('class'=>'btn btn-success', 'id' => 'submitButton', 'disabled' => 'disabled')) !!}
				                    </div>
			                    </div>
		                    
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- form -->
  
        @else

            <!-- Texto Caducidad Field -->
            <div class="form-group">
                <h3 class="text-center">{!! $landings->texto_caducidad !!}</h3>
            </div>

        @endif

    </div>

@endsection
