@extends('main')
@section('title', '+VIDA - Recursos Humanos')
@section('content')
<div class="container-fluid">
	<div class="row">
		
		<div id="carousel-filosofia" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-home-01.png" alt="RECURSOS HUMANOS. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner">
							<p>RECURSOS HUMANOS</p>
							<p><img src="/css/img/copy-empresas.png" alt="RECURSOS HUMANOS. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							<a class="btn btn-primary" href="#form">CARGAR CV <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey">
			<div class="container text-center from-bottom">
				<h2>CARGA DE CURRICULUM VITAE</h2>

				<p>En <strong class="red">+VIDA</strong> ofrecemos productos y servicios de calidad buscando constantemente propuestas innovadoras para lograr una mayor satisfacción de nuestros clientes.</p>
				<p>Somos <strong>la empresa líder en Servicios Médicos Domiciliarios en la Zona Norte de GBA.<br />Emergencias Médicas, Medicina Laboral e Internación Domiciliaria.</strong></p>
				<p>Nos destacamos por la calidez humana y el buen trato al personal, y en el estar cerca de nuestros pacientes.</p>
				<p>Siempre estamos en la búsqueda de profesionales de la salud, que les interese sumarse a nuestro equipo profesional, que trabajen con pasión por la gente, proactividad y flexibilidad para adaptarse a los cambios, con ganas de asumir nuevos desafíos y que compartan nuestros valores de responsabilidad, cercanía, compromiso, innovación y entusiasmo.</p>
				<p>En <strong class="red">+VIDA</strong> fomentamos tu desarrollo personal y tu formación profesional.</p>
				<p>Sumate al equipo de <strong class="red">+VIDA</strong></p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.cv')
			</div>
		</div>
	</div>
</div>
</div>
@endsection
