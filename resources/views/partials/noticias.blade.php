@extends('main')
@section('title', '+VIDA - Noticias')
@section('content')


<div class="novedades col-xs-12">
	

	@if(count($noticias) > 0)
		<!--<div class="row">
			<select id="orden" class="select-orden pull-right"action ='/noticias-order'> 
			    <option value="desc" @if($order == 'desc')selected @endif>MAS RECIENTES</option>
			    <option value="asc" @if($order == 'asc')selected @endif>MAS ANTIGUAS</option>
			</select>
			<input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
		</div>-->



		<div >
		<?php $count = 1 ; $flag = 1; $elements = 0; $page = 1?>
		@foreach($noticias as $noticia)

			<!-- abre pagina cada 8 noticias -->
			@if($elements %8 == 0 or $elements == '0' )
				<div id ="pagina-{{$page}}" class="paginas-noticias">	
			@endif
			
			<!-- abre row cada 4 noticias -->
			@if($count == '1')
				<div class="row" >	
			@endif

				<div class="col-sm-6 col-md-3">
		
						<a href="/novedad/{!! $noticia->url !!}" class="hidden-lg">
							<div class="panel panel-noticias" style="background-image: url({!! URL::asset('/uploads/noticias/'.$noticia->image_thumb) !!})">
								<div class="caption-small">
									<h4 class="text-uppercase">{!! $noticia->titulo !!}</h4>
							        <h6 class="sub-titulo text-uppercase">{!! $noticia->sub_titulo !!}</h6>
								</div>
							</div>
						</a>
						<div class="panel panel-noticias hidden-sm hidden-md hidden-xs" style="background-image: url({!! URL::asset('/uploads/noticias/'.$noticia->image_thumb) !!})">
							<div class="caption">
								<h4 class="text-uppercase">{!! $noticia->titulo !!}</h4>
							    <h6 class="sub-titulo text-uppercase">{!! $noticia->sub_titulo !!}</h6>
							    <div class="descripcion desc-small">{!! str_limit($noticia->descripcionTextoPlano, 110) !!}</div>
								<div class="noticia-text text-center" style="display:none">
									<span class="descripcion" >{!! str_limit($noticia->descripcionTextoPlano, 250) !!}</span>
									<a href="/novedad/{!! $noticia->url !!}"><h3 class="know">VER MAS</h3></a>
								</div>

							</div>
						</div>
		
				</div>

			<!-- cierra row cada 4 noticias -->
			@if($count == '4' || $flag == count($noticias) )
				</div>
				<?php $count = 0 ; ?>
			@endif

			<?php $count ++; $flag ++; $elements ++;?>

			<!-- cierra pagina cada 8 noticias -->
			@if($elements %8 == 0 and $elements != 0)
				</div>
			@endif
			@if($elements %8 == 0 )
				<?php $page ++;?>	
			@endif
			
	 	@endforeach
	 	</div>

	 	<div class="col-xs-12"> <div class="pull-right">{!! $noticias->render() !!}</div></div>

	@else
		No se encontraron noticias.
 	@endif

   </div>        
   </div> 
@endsection

@section('viewscripts')
    <script src="{{ asset('js/frontend/noticias.js') }}"></script>
@endsection

