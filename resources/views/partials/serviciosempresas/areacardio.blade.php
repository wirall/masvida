@extends('main')
@section('title', '+VIDA - Área cardio')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div id="carousel-area-cardio" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-areacardio-01.png" alt="ESTABLECIMIENTO CARDIO PROTEGIDO. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner">
							<p>ESTABLECIMIENTO CARDIO PROTEGIDO</p>
							<p><img src="/css/img/copy-empresas.png" alt="ESTABLECIMIENTO CARDIO PROTEGIDO. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							<a class="btn btn-primary" href="#form">COTIZAR PLAN <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey">
			<div class="container">
				<h2 class="text-center from-bottom">CERTIFICACIÓN ÁREA CARDIO <span class="red">MÁS VIDA</span></h2>
				
				<p class="lead text-center from-bottom">CONTRATE CON <strong class="red">MÁS VIDA</strong> LA CAPACITACIÓN, EL EQUIPAMIENTO, EL ÁREA PROTEGIDA Y LA METODOLOGÍA PARA CONVERTIR SU ESTABLECIMIENTO EN UN ÁREA CARDIO ASEGURANDO LA RÁPIDA ATENCIÓN EN CASO DE <strong>RIESGO DE VIDA</strong> ANTE UNA <strong>EMERGENCIA</strong> DE ÍNDOLE <strong>CARDÍACA.</strong></p>

				<div class="row">
					
					<div class="col-sm-6 from-left">
						<p>Es una validación que emite <strong class="red">+VIDA</strong>, apoyado en estándares internacionales, para instituciones que posean protocolos de actuación ante eventos de muerte súbita, primeros respondientes entrenados en técnicas de <strong>Reanimación Cardio Pulmonar (RCP), Desfibriladores Automáticos Externos (DEA)</strong> y una empresa que les brinde el servicio de <strong>Área Protegida</strong>. Las personas formadas deberán ser un número suficiente como para poder cubrir dicha área durante el horario de prestación de servicios y/o de realización de actividad.</p>
						<p>La certificación <strong class="red">Área Cardio</strong> es un eficiente instrumento puesto al servicio de su establecimiento para el cuidado de empleados, clientes, público en general y en especial usted y sus seres queridos.</p>
					</div>
					
					<div class="col-sm-6 from-right">
						<p>La certificación <strong class="red">Área Cardio</strong>, está conformada por <strong>un conjunto de normas y protocolos que incluyen capacitación de los primeros respondientes</strong> para que actúen de forma organizada y eficiente ante un evento que requiera de su intervención hasta el arribo del personal médico de emergencia.</p>
						<p>Poseemos <strong>profesionales altamente capacitados y acreditados internacionalmente</strong> para asesorarlo y acompañarlo a transitar cada etapa.</p>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-info text-center">
			<div class="container">
				<h2 class="from-top">REQUISITOS PARA PODER CERTIFICAR COMO UN "ÁREA CARDIO"</h2>
				<div class="col-sm-4 from-bottom">
					<h5>PRIMEROS RESPONDIENTES</h5>
					<ul>
						<li>Personal entrenado en <strong>RCP</strong> y manejo de <strong>DEA</strong>.</li>
						<li>En número suficiente para cubrir todos los horarios y turnos de funcionamiento de la institución.</li>
					</ul>
				</div>
				<div class="col-sm-4 from-top">
					<h5>DESFIBRILADORES AUTOMÁTICOS</h5>
					<ul>
						<li>Equipos con habilitación ANMAT.</li>
						<li>Estratégicamente distribuidos.</li>
						<li>Señalizada su localización.</li>
						<li>Poseer un plan de mantenimiento.</li>
					</ul>
				</div>
				<div class="col-sm-4 from-bottom">
					<h5>SERVICIO DE ÁREA PROTEGIDA</h5>
					<ul>
						<li>Contar con la cobertura de una empresa de emergencias médicas reconocida.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro">
			<div class="container text-center from-bottom">
				<h2><span class="red">ÁREA CARDIO</span> ESTARÁ IDENTIFICADA MEDIANTE:</h2>
				
				<ul>
					<li>Localización de <strong>DEA (Desfibriladores Externos Automáticos)</strong> en planos de evacuación u otros.</li>
					<li>Carteles indicadores con instrucciones de uso de <strong>DEAs.</strong></li>
					<li>Carteles indicadores DEA sobre cada equipo disponible.</li>
					<li>Placa identificativa que será colocada tras conseguir la certificación de <strong class="red">Área Cardio Más Vida</strong> en el acceso principal del establecimiento, para conocimiento de los usuarios de los servicios y del público en general.</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey">
			<div class="container text-center from-bottom">
				<h2>BENEFICIOS DE TENER LA CERTIFICACIÓN <strong class="red">ÁREA CARDIO MÁS VIDA</strong></h2>
				
				<p><strong class="red">La mayoría de las muertes ocurren fuera del ámbito hospitalario</strong> <strong>(más del 80%)</strong> y el tiempo de respuesta es el mayor determinante en el pronóstico de sobrevida. Es por ello que si su institución se encuentra equipada y con un protocolo de actuación ante situaciones de emergencias, usted y toda persona que se encuentre dentro de la misma estará más seguro y protegido.</p>
				<p>Una vez concluido el circuito para la certificación usted recibirá un diploma, una placa y todo el material necesario para identificar a su institución como un <strong class="red">Área Cardio Más VIDA</strong>.</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro">
			<div class="container text-center from-bottom">
				<h2>QUIENES PUEDEN SER PARTE</h2>
				
				<p>Todos aquellos espacios públicos o privados de todo el territorio Nacional, que presenten gran concurrencia de personas fijas o en tránsito, o aquellos que independientemente de la cantidad de personas, quieran convertir su lugar/institución en un <strong class="red">Área Cardio</strong> y que cuenten con:</p>
				
				<div class="row text-left">
					<div class="col-sm-6 from-right">
						<ul>
							<li>Parque industriales, parques empresariales y centros de negocios.</li>
							<li>Empresas industriales y de servicios (entidades financieras, oficinas, etc.).</li>
							<li>Instituciones educativas (Universidades, Institutos, Escuelas y Colegios).</li>
							<li>Residencias Geriátricas y centros de día.</li>
							<li>Restaurantes, bares, establecimientos bailables.</li>
							<li>Gimnasios.</li>
							<li>Clubes e instituciones deportivas (Polideportivos, campos de golf, campos de fútbol, etc).</li>
							<li>Centros de estética, SPAs, etc.</li>
							<li>Barrios privados/ Countries.</li>
						</ul>
					</div>
					<div class="col-sm-6 from-left">
						<ul>
							<li>Sanatorios y/o hospitales.</li>
							<li>Locales destinados a espectáculos (cines y teatros).</li>
							<li>Centros comerciales, Shoppings.</li>
							<li>Salas de conferencias, eventos o exposiciones.</li>
							<li>Establecimientos dependientes de la Administración Pública como asimismo establecimientos privados.</li>
							<li>Casinos, bingos, hoteles y distintos lugares de esparcimiento.</li>
							<li>Terminales de transporte fluvial, aéreo y terrestre, urbano nacional e internacional.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.servicios')
			</div>
		</div>
	</div>
</div>
<!-- Google Code para etiquetas de remarketing -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 941117892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/941117892/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
@endsection
