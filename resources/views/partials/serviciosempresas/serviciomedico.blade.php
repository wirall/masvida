@extends('main')
@section('title', '+VIDA - Servicio médico')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div id="carousel-servicio-medico" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-serviciomedico-01.png" alt="SERVICIO DE MÉDICO Y ENFERMERÍA EN PLANTA. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner">
							<p>SERVICIO DE MÉDICO Y ENFERMERÍA EN PLANTA</p>
							<p><img src="/css/img/copy-empresas.png" alt="CONVENIO Y PLANES PARA EMPLEADOS. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							<a class="btn btn-primary" href="#form">COTIZAR PLAN <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey from-bottom">
			<div class="container text-center">
				<h2>SERVICIO DE MÉDICO Y ENFERMERÍA EN PLANTA</h2>
				<p>Aseguramos la cobertura requerida por ley, con Personal Profesional incluyendo los reemplazos de vacaciones, licencias prolongadas, enfermedad y otras.</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-info panel-complementarios text-center">
			<div class="container">
				<h2>COORDINACIÓN DE ESTUDIOS COMPLEMENTARIOS PARA DIAGNÓSTICOS QUE SU DEPARTAMENTO MÉDICO REQUIERA</h2>

				<div class="row">
					<div class="col-xs-6 col-sm-4 item-7 from-top">
						<div class="thumbnail">
							<img src="/css/img/icon-imagenes.png" alt="IMÁGENES">
							<div class="caption-filosofia">
								<h5>IMÁGENES</h5>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 item-7 from-bottom">
						<div class="thumbnail">
							<img src="/css/img/icon-analisisclinicos.png" alt="ANÁLISIS CLÍNICOS">
							<div class="caption-filosofia">
								<h5>ANÁLISIS CLÍNICOS</h5>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 item-7 from-top">
						<div class="thumbnail">
							<img src="/css/img/icon-analisisespecificos.png" alt="ANÁLISIS ESPECÍFICOS">
							<div class="caption-filosofia">
								<h5>ANÁLISIS ESPECÍFICOS</h5>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 item-7 from-bottom">
						<div class="thumbnail">
							<img src="/css/img/icon-adicciones.png" alt="TRATAMIENTO EN ADICCIONES">
							<div class="caption-filosofia">
								<h5>TRATAMIENTO EN ADICCIONES</h5>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 item-7 from-top">
						<div class="thumbnail">
							<img src="/css/img/icon-interconsulta.png" alt="INTERCONSULTA CON MÉDICOS ESPECIALISTAS">
							<div class="caption-filosofia">
								<h5>INTERCONSULTA CON MÉDICOS ESPECIALISTAS</h5>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 item-7 from-bottom">
						<div class="thumbnail">
							<img src="/css/img/icon-kinesiologia.png" alt="KINESIOLOGÍA">
							<div class="caption-filosofia">
								<h5>KINESIOLOGÍA</h5><br>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 item-7 from-top">
						<div class="thumbnail">
							<img src="/css/img/icon-nutricion.png" alt="NUTRICIÓN">
							<div class="caption-filosofia">
								<h5>NUTRICIÓN</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro">
			<div class="container">
				<div class="col-sm-4 from-bottom">
					<p><strong>Proyecto, gestión y dirección del departamento médico y/o enfermería para su empresa / planta industrial.</strong></p>
					<p>Le brindamos todos los servicios requeridos por ley, y los adicionales que su empresa defina, para asegurar la salud ocupacional de su gente.</p>
				</div>
				<div class="col-sm-4 from-bottom">
					<p><strong>Armado y equipamiento de enfermerías y/o consultorios.</strong></p>
					<p>Le proveemos todos los equipamientos e insumos médicos para las enfermerías y consultorios para su empresa.</p>
				</div>
				<div class="col-sm-4 from-bottom">
					<p>Cobertura médica con ambulancias <strong>UTIM</strong> en su establecimiento, en los días y horarios que requiera (hasta 24 hs. los 7 días de la semana).</p>
				</div>
				<div class="col-sm-5 col-sm-offset-7 from-bottom">
					<p class="text-right">
						<a href="{{ url('serviciosempresas/medicinalaboral') }}">LEER TAMBIÉN MEDICINA LABORAL <i class="fa fa-arrow-circle-right"></i></a>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.servicios')
			</div>
		</div>
	</div>
</div>
@endsection
