@extends('main')
@section('title', '+VIDA - Planes')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div id="carousel-planes" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-planes-01.png" alt="CONVENIO Y PLANES PARA EMPLEADOS. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner">
							<p>CONVENIO PARA EMPLEADOS.</p>
							<p><img src="/css/img/copy-empresas.png" alt="CONVENIO Y PLANES PARA EMPLEADOS. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							<a class="btn btn-primary" href="#form">COTIZAR PLAN <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey from-bottom">
			<div class="container text-center">
				<h2>CONVENIO Y PLANES PARA EMPLEADOS</h2>
				<p>Otorgue un <strong>beneficio adicional</strong> para sus empleados y su familia, con un Plan para cada necesidad.<br />
				Este puede incluir <strong>cobertura de Emergencias y Urgencias Médicas, médico a domicilio</strong> (adulto y pediátrico), descuentos en consultorios y farmacias, <strong>atención odontológica y cobertura Nacional e Internacional</strong> por Universal Assistance.<br />
				En <strong class="red">+VIDA</strong> desarrollamos los mejores planes para su empresa, con variadas opciones y amplios servicios. Encontrará el <strong>Plan de asistencia especializada</strong> en emergencias y visitas médicas ideal para usted, su empleado y su familia.<br />
				Puede optar además por descuentos en centros médicos, centros de estudios y diagnósticos,  farmacias y ópticas.</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="container block-container">
			
			<div class="panel panel-info from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-urgenciasmedicas.png" alt="EMERGENCIAS Y URGENCIAS MÉDICAS">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">EMERGENCIAS Y URGENCIAS MÉDICAS</h4>
    					<p>Ante un riesgo de vida, contamos con una amplia flota de Unidades de Terapia Intensiva Móvil de última generación, distribuidas estratégicamente para asistir a nuestros socios en el menor tiempo posible con toda la tecnología, profesionales capacitados y la rapidez suficiente. En el caso de ser necesario, se realizará el traslado o derivación del paciente al centro asistencial más cercano o el que le correspondiera, siempre que el profesional lo considere conveniente para una eficaz atención.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-primary from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-paiseslimitrofes.png" alt="COBERTURA EN PAÍSES LIMÍTROFES">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">COBERTURA EN PAÍSES LIMÍTROFES</h4>
    					<p>Cuando hablamos de cuidar personas sabemos que el nivel de exigencia debe ser superlativo, más aún cuando nuestros clientes se encuentran lejos de sus casas, viajando por diferentes partes del país o los países limítrofes.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-info from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-medicoadomicilio.png" alt="MÉDICO A DOMICILIO">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">MÉDICO A DOMICILIO</h4>
    					<p>Tenga el médico en su casa cuando lo necesite. No sólo para personas imposibilitadas de salir del domicilio, sino también por comodidad y atención a toda hora sin tener que movilizarse a un centro médico o pedir turnos para la correspondiente atención.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-primary from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-cartilladebeneficios.png" alt="CARTILLA DE BENEFICIOS">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">CARTILLA DE BENEFICIOS</h4>
    					<p>Poseemos una amplia red de descuentos en centros médicos, especialistas, farmacias, centros de diagnóstico y más. Dentro de los beneficios contamos con una amplia cartilla dividida por zonas geográficas, centros médicos, profesionales independientes,  odontólogos, farmacias y ópticas, a los que se pueden acceder presentando la credencial con un arancel preferencial.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-info from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-centrosmedicos.png" alt="CENTROS MÉDICOS">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">CENTROS MÉDICOS</h4>
    					<p>Presentando nuestra credencial los asociados tendrán acceso a una variada y completa cartilla de centros médicos y de diagnóstico dentro del área de cobertura, con aranceles preferenciales. Los mismos poseen diferentes especialidades médicas y guardias, para una excelencia en la atención.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-primary from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-medicosespecialistas.png" alt="MÉDICOS ESPECIALISTAS">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">MÉDICOS ESPECIALISTAS</h4>
    					<p>Con nuestra credencial y con un arancel preferencial, los asociados podrán realizar consultas particulares en consultorios con médicos clínicos, pediatras, ginecólogos, cardiólogos y otras especialidades.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-info from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-descuentosenfarmacias.png" alt="DESCUENTOS EN FARMACIAS">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">DESCUENTOS EN FARMACIAS</h4>
    					<p>Los socios de +VIDA cuentan con el beneficio de tener descuento presentando la credencial en los establecimientos adheridos a la red y publicados en la cartilla, para todo tipo de medicamentos, productos y accesorios no crónicos.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-primary from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-coberturaodontologica.png" alt="COBERTURA ODONTOLÓGICA">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">COBERTURA ODONTOLÓGICA</h4>
    					<p>Nuestros socios cuentan con el beneficio de una amplia cobertura odontológica. Con la presentación de la credencial en los centros seleccionados podrán realizarse arreglos, extracciones y tratamientos de endodoncia, como también recibirán descuentos en tratamientos de mayor complejidad, ortodoncia y prótesis dentales.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-info from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-urgenciasodontologicas.png" alt="URGENCIAS ODONTOLÓGICAS">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">URGENCIAS ODONTOLÓGICAS</h4>
    					<p>En los casos de malestar, molestias o dolores, como también accidentes o roturas de piezas dentarias, nuestros asociados cuentan con centros odontológicos de atención las 24hs. los 365 días del año, como así también atención domiciliaria de un médico para aplicar un paliativo al dolor.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-primary from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-autoprotegido.png" alt="AUTO PROTEGIDO">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">AUTO PROTEGIDO</h4>
    					<p>Aún dentro de su vehículo usted y todas las personas que estén dentro de él, sean o no socios de +VIDA, están protegidos en caso de un accidente que ocurra en CABA y GBA, con un una identificación o sticker distintivo.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-info from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-enfermeria.png" alt="ENFERMERÍA">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">ENFERMERÍA</h4>
    					<p>El asociado cuenta con un servicio de enfermería en su casa, para resolver casos tales como administración de medicación e inyectables, control de signos vitales, higiene y confort o curaciones simples y complejas, aprovechando en todos los casos la contención que brinda el ámbito familiar.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-primary from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-kinesiologia-2.png" alt="KINESIOLOGÍA">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">KINESIOLOGÍA</h4>
    					<p>Ser socio de +VIDA permite acceder a sesiones de kinesiología a domicilio, un beneficio importante para personas con movilidad reducida. </p>
					</div>
				</div>
			</div>

			<div class="panel panel-info from-bottom">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-trasladosprogramados.png" alt="TRASLADOS PROGRAMADOS">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">TRASLADOS PROGRAMADOS</h4>
    					<p>Con un costo preferencial, este servicio cubre la necesidad de los pacientes que tienen que movilizarse desde o hacia su domicilio, programado con anticipación y contando con todas las especificaciones médicas pertinentes para el traslado según el requerimiento o la patología del paciente.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-primary ">
				<div class="media">
					<div class="media-left">
					    <a href="#">
					      	<img class="media-object" src="/css/img/icon-hogarprotegido.png" alt="HOGAR PROTEGIDO">
					    </a>
  					</div>
					<div class="media-body">
    					<h4 class="media-heading">HOGAR PROTEGIDO</h4>
    					<p>El asociado cuenta con el servicio de protección del hogar y de toda persona que se encuentre dentro del mismo, sean o no socios de +Vida. También contamos con un servicio de asistencia del hogar que cubre urgencias de plomería, gas, electricidad, cerrajería y cristales las 24 hs. los 365 días del año.</p>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.servicios')
			</div>
		</div>
	</div>
</div>
@endsection
