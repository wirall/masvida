@extends('main')
@section('title', '+VIDA - Área protegida')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div id="carousel-area-protegida" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-areaprotegida-01.png" alt="ÁREA PROTEGIDA. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner">
							<p>ÁREA PROTEGIDA</p>
							<p><img src="/css/img/copy-empresas.png" alt="ÁREA PROTEGIDA. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							<a class="btn btn-primary" href="#form">COTIZAR PLAN <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey from-bottom">
			<div class="container text-center">
				
				<h2>ÁREA PROTEGIDA</h2>
				<p>Este servicio le brinda Emergencias y Urgencias a toda persona que se encuentre dentro de un predio, ya sea una empresa, comercio, barrios, colegios, instituciones, etc.<br />
				Protegemos  a empleados,  visitas, proveedores,  o toda persona que se encuentre  eventualmente o de forma permanente dentro del lugar, <strong>las 24 horas, los 365 días del año</strong>.</p>
				<p>Para afrontar las necesidades que tiene el mercado actual, <strong class="red">+VIDA</strong> está especializada en servicios para empresas, comercios e instituciones, con planes diseñados para cada necesidad en particular.<br />
				Brindamos la cobertura en emergencias y urgencias médicas cuidando lo más importante en una empresa: el capital humano.</p>
				
				<div class="row">
					<div class="col-sm-6 from-left">
						<div class="well">
							<p><strong class="red">+ VIDA</strong> atenderá los casos donde exista riesgo de vida, pérdida de un órgano vital, paros cardíacos, fracturas expuestas, quemaduras, etc. o toda situación donde sea necesaria una atención médica urgente e inmediata.</p>
						</div>
					</div>
					<div class="col-sm-6 from-right">
						<div class="well">
							<p>En el caso que por criterio del profesional médico actuante el paciente deba derivarse a un centro asistencial, el mismo será trasladado al hospital más cercano dentro de la zona, o a donde su cobertura médica lo autorice.</p>
						</div>
					</div>
				</div>
				
				<p class="from-bottom"><strong class="red">+ VIDA</strong> posee Ambulancias de tipo <strong>UTIM</strong> (Unidad de Terapia Intensiva Móvil) de última generación con todo el equipamiento y con la máxima tecnología para casos de emergencias, preparadas para responder de forma inmediata, mejorando las posibilidades de sobrevida de un paciente.</p>
				<p class="from-bottom"><strong>Personal experimentado:</strong> Poseemos personal altamente calificado capaz de responder de forma eficiente e inmediata ante una emergencia/urgencia médica. Contamos con el principal activo para brindar prestaciones de calidad, la capacitación permanente orientada a todo el  personal, a través de nuestro Departamento de Docencia.</p>
				<p class="from-bottom"><strong class="red">El servicio se activa con su llamada.</strong> Mientras la ambulancia está en camino al lugar solicitado, nuestro operador le hará algunas preguntas para que cuando llegue el médico esté interiorizado de lo sucedido. Si fuera necesario, también lo asistirá telefónicamente para que pueda contener a la persona  en riesgo.</p>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-info text-center from-bottom">
			<div class="container">
				<p><strong class="red">+VIDA</strong> posee un Cuerpo Directivo Médico, actualmente con siete integrantes profesionales de la medicina, especializados en la gestión de la salud pública y privada, junto a un equipo de gestión con años de experiencia en la salud ambulatoria.</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.servicios')
			</div>
		</div>
	</div>
</div>
<!-- Google Code para etiquetas de remarketing -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 941117892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/941117892/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
@endsection
