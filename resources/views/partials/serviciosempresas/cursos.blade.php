@extends('main')
@section('title', '+VIDA - Cursos')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div id="carousel-cursos" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-cursos-01.png" alt="CURSOS. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner">
							<p>CURSOS</p>
							<p><img src="/css/img/copy-empresas.png" alt="CONVENIO Y PLANES PARA EMPLEADOS. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							<a class="btn btn-primary" href="#form">COTIZAR PLAN <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>
	</div>

	<div class="row">
		<div class="intro">
			<div class="container text-center">
				<h2>CURSOS</h2>
				<p>El <strong>departamento de Docencia</strong> tiene la misión de educar y capacitar a la comunidad en temas de salud, tales como prevención de enfermedades y lesiones y a accionar ante situaciones de emergencias. El mismo cuenta con personal formado y acreditado para tal fin, así como todo el material necesario para el correcto aprendizaje teórico y práctico.</p>
				<p>Contamos con el aval de la <strong class="red">Asociación Americana del Corazón</strong> conocida mundialmente como <strong>American Heart Association (AHA)</strong>, instituida con más de <strong class="red">90 años de trayectoria</strong> y reconocida internacionalmente en temas de docencia e investigación en la emergencia cardio-pulmonar <strong>(RCP)</strong>.</p>
			</div>
			
			<div class="container from-bottom">
				<ul class="col-sm-10 col-sm-offset-1 logos-container text-center">
					<li><img class="img-responsive" src="/css/img/logo-aha.png" alt="American Heart Association"></li>
					<li><img class="img-responsive" src="/css/img/logo-siem.png" alt="siem"></li>
					<li><img class="img-responsive" src="/css/img/logo-masvida-small.png" alt="+VIDA"></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey">
			<div class="container text-center from-bottom">
				<h3 class="red">LOS CURSOS SE BRINDAN EN VUESTRAS INSTALACIONES, O BIEN EN LAS INSTALACIONES DE MÁS VIDA</h3>
				<h2>CURSOS CON LA CERTIFICACIÓN DE LA AMERICAN HEART ASSOCIATION AHA</h2>
			</div>

			<div class="container block-container">
				<div class="cursos-container from-bottom">
					<h5 class="bg-primary">FAMILIARES Y AMIGOS</h5>
					<h6 class="bg-info red">APRENDA LAS HABILIDADES PARA SALVAR VIDAS GUIADOS POR UN INSTRUCTOR CERTIFICADO DE LA AHA</h6>

					<div class="col-sm-6">
						<p>El curso <strong class="red">Familiares y Amigos RCP</strong> enseña la <strong>RCP Reanimación Cardio Pulmonar) Hands Only™</strong> o ®(solo con las manos), el uso del DEA en adultos, la RCP y el uso del <strong>DEA en niños</strong> y la <strong>RCP en lactantes.</strong><br />Durante el curso también se aprenderá como aliviar obstrucción de la vía aérea en un adulto, niño o lactante.</p>
						<p>Las habilidades se enseñan mediante la técnica de la <strong>AHA</strong> "practicar mientras mira", demostrada científicamente, esta técnica permite a los participantes practicar las habilidades mientras observan modelos precisos y coherentes de actuación en el video del curso. Las habilidades en adultos, niños y lactantes se presentan en tres módulos independientes para permitir una mayor flexibilidad.</p>
					</div>
					<div class="col-sm-6">
						<p>El curso <strong class="red">Familiares y Amigos RCP</strong> será impartido por instructores certificados y será acompañado de un <strong>Certificado de asistencia y participación</strong> expedido por <strong class="red">+VIDA</strong>.</p>
						<p class="bg-info">Aproximadamente el <strong>80%</strong> de las muertes súbitas ocurren en el ámbito familiar y/o recreacional. El <strong>92%</strong> de esas víctimas no sobrevive, generalmente porque las personas que las rodean no intervienen y <strong>no</strong> comienzan la <strong>RCP</strong>. Usted puede ayudar a cambiar las estadísticas aprendiendo estas sencillas habilidades de <strong>RCP</strong>.</p>
						<p><strong>Destinatarios:</strong> Público en general.<br /><strong>Duración:</strong> 3 horas.<br />Arancelado. Pedir presupuesto.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro">
			<div class="container">	
				<div class="cursos-container from-bottom">
					<h5 class="bg-primary">PRIMEROS AUXILIOS RCP Y DEA</h5>
					
					<div class="col-sm-6">
						<p>El curso de <strong class="red">Heartsaver</strong> <strong>primeros auxilios con RCP y DEA</strong>  es un curso presencial, video guiado y dirigido por un instructor, que enseña las habilidades y conocimientos fundamentales para responder y tratar en los primeros minutos y hasta la llegada del servicio de emergencias médicas <strong>(SEM)</strong>, situaciones tales como el atragantamiento, paro cardiorrespiratorio súbito y otras situaciones que requieran de primeros auxilios. En este curso los estudiantes aprenden las habilidades para tratar hemorragias, esguinces, fracturas óseas, shock y otras emergencias de primeros auxilios. También se enseña <strong>RCP</strong> para adultos y el uso de <strong>DEA</strong>.</p>
						<p>El curso <strong class="red">Heartsaver</strong> <strong>primeros auxilios con RCP y DEA</strong> enseña las habilidades con la técnica de la <strong>AHA</strong> "practicar mientras mira", demostrada científicamente, esta técnica permite a los instructores observar a los estudiantes, hacer comentarios y guiar la adquisición de habilidades.</p>
					</div>
					<div class="col-sm-6">
						<p class="bg-info">Al finalizar y aprobar el curso (lo que incluye una demostración de habilidades de primeros auxilios y la prueba de habilidades de <strong>RCP</strong> y <strong>DEA</strong>), los estudiantes reciben una tarjeta que acreditará que han completado el curso <strong>con un certificado de</strong> <strong class="red">Heartsaver</strong> <strong>primeros auxilios</strong> con RCP y DEA, con una validez de <strong>2 años</strong>.</p>
						<p><strong>Destinatarios:</strong> Toda persona interesada en adquirir conocimientos en RCP-Primeros Auxilios y/o aquellos que necesiten un certificado que acredite su formación.<br /><strong>Duración:</strong> 8 horas.<br />Arancelado. Pedir presupuesto.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey">
			<div class="container">	
				<div class="cursos-container from-bottom">
					<h5 class="bg-primary">SOPORTE VITAL BÁSICO (SVB – BLS)</h5>
					<h6 class="bg-info red">PARA PROFESIONALES DE LA SALUD</h6>

					<div class="col-sm-6">
						<p><strong>El curso de SVB/BLS para profesionales de la salud  es un curso presencial, video guiado y dirigido por un instructor</strong> donde se enseñan las habilidades de soporte vital básico para un solo reanimador y para un equipo. En este curso los participantes aprenderán a reconocer rápidamente varias situaciones de emergencia en las que hay vidas en peligro, realizar compresiones torácicas de alta calidad, administrar ventilaciones apropiadas y utilizar con prontitud un <strong>DEA</strong>.</p>
					</div>
					<div class="col-sm-6">
						<p class="bg-info"><strong>La formación sobre SVB/BLS</strong> de la <strong class="red">American Heart Association</strong> es completamente innovadora, no sólo porque el contenido está basado en hechos, sino porque usa una metodología totalmente comprobada que puede mejorar considerablemente el entrenamiento como la retención de habilidades de reanimación.</p>
						<p><strong>Destinatarios:</strong> Profesionales de la salud.<br /><strong>Duración:</strong> Jornada única de 5 horas.<br />Arancelado. Pedir presupuesto.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro">
			<div class="container text-center from-bottom">
				<h2>CURSOS CON LA CERTIFICACIÓN <strong>MÁS VIDA</strong></h2>
			</div>

			<div class="container block-container">
				<div class="cursos-container from-bottom">
					<h5 class="bg-primary">CURSO BÁSICO DE RCP PARA COLEGIOS Y ENTIDADES EDUCATIVAS</h5>
					<h6 class="bg-info red">¿TENÉS LO NECESARIO PARA SALVAR UNA VIDA?</h6>

					<div class="col-sm-6">
						<p>En esta charla se aprenderá como hacer frente a cualquier tipo de incidente médico hasta el arribo del personal calificado, pero sin aprender el accionar en temas de <strong>Primeros Auxilios</strong> y <strong>RCP</strong>.</p>
						<p>El temario de la misma es:<br /> • Tipos de servicios médicos.<br /> • Pasos a seguir en una Emergencia.<br /> • El llamado de Emergencia.<br /> • Pacientes adultos (principales causas de muerte):<br />- Enfermedad cardiovascular.</p>
					</div>
					<div class="col-sm-6">
						<p> • Paciente pediátrico (principales causas de muerte):<br />- Lesiones traumáticas.</p>
						<p><strong>Destinatarios:</strong> principalmente instituciones educativas, empresas, fábricas y todas aquellas instituciones/instalaciones que alberguen gran número de personas, fijas o en tránsito.<br /><strong>Duración:</strong> 60 minutos.<br /><strong>Modalidad:</strong> teórica<br /><strong>Valor:</strong> sin cargo.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey">
			<div class="container">	
				<div class="cursos-container from-bottom">
					<h5 class="bg-primary">MANEJO DE TRAUMA</h5>
									
					<div class="col-sm-6">
						<p>En este curso se aprenderá como manejar a un paciente traumatizado, como brindarle la primera asistencia hasta el arribo de personal más calificado.</p>
						<p>Temario:<br />• Manejo de una escena, seguridad en la escena.<br />• Cuando actuar y cuando no actuar.<br />• Manejo básico del paciente poli-traumatizado.</p>
					</div>

					<div class="col-sm-6">
						<p>• Como inmovilizar y trasladar al paciente.<br />• Nociones de Bioseguridad.</p>
						<p><strong>Destinatarios:</strong> Personal de la Salud, Brigadistas, etc.<br /><strong>Duración:</strong> 4 horas.<br /><strong>Modalidad:</strong> Teórica y práctica.<br /><strong>Valor:</strong> Arancelado. (Pedir presupuesto.)</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro">
			<div class="container">	
				<div class="cursos-container from-bottom">
					<h5 class="bg-primary">ATENCIÓN DE LLAMADAS DE EMERGENCIA, CONTENCIÓN, INSTRUCCIONES DE PRE-ARRIBO, ASISTENCIA TELEFÓNICA.</h5>
					
					<div class="col-sm-6">
						<p>Durante el mismo se aprenderá cómo atender una llamada de emergencia, como calificar un servicio (Triage), como brindar contención al solicitante y como brindar instrucciones de pre-arribo.  </p>
						<p><strong>Destinatarios:</strong> Personal de una empresa de emergencias médicas.<br /><strong>Duración:</strong> 54 horas.<br /><strong>Modalidad:</strong> Teórica y práctica.<br /><strong>Valor:</strong> Arancelado. (Pedir presupuesto.)</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.servicios')
			</div>
		</div>
	</div>
</div>
<!-- Google Code para etiquetas de remarketing -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 941117892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/941117892/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
@endsection
