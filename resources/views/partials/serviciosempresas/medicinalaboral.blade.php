@extends('main')
@section('title', '+VIDA - Medicina laboral')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div id="carousel-medicina-laboral" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-medicinalaboral-01.png" alt="MEDICINA LABORAL. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner">
							<p>MEDICINA LABORAL</p>
							<p><img src="/css/img/copy-empresas.png" alt="MEDICINA LABORAL. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							<a class="btn btn-primary" href="#form">COTIZAR PLAN <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey">
			<div class="container text-center from-bottom">
				<h2>MEDICINA LABORAL</h2>
				<p>La tendencia actual es generar una relación entre trabajo, ámbito laboral y salud. La medicina laboral va más allá de la cobertura de enfermedades o accidentes de trabajo: apunta a proteger la salud de sus trabajadores al ocuparse también de la prevención, los factores de riesgo y la adecuación del empleado al puesto de trabajo.</p>
				<p><strong class="red">+VIDA</strong> ofrece diferentes variantes y soluciones para asegurar el bienestar de las personas que trabajan en su empresa. Los diferentes servicios van variando según cuál sea la necesidad de cada empresa y la actividad que desarrolla. Todas están diseñadas para optimizar y proteger el funcionamiento del capital humano de la organización.</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro">
			<div class="container from-bottom">
				<h2 class="text-center">SERVICIOS PARA EMPRESAS:</h2>
				<ul class="col-sm-6">
					<li>Control de Ausentismo.</li>
					<li>Exámenes en salud (Exámenes pre-ocupacional, periódico, de reincorporación por ausencia prolongada, por cambios de tareas, exámenes de egreso).</li>
					<li>Chequeo Ejecutivo.</li>
					<li>Libreta Sanitaria.</li>
					<li>Recalificación Laboral / Profesional.</li>
					<li>Soluciones para medicina empresarial en el interior del país.</li>
					<li>Equipo personalizado de profesionales (Ortopedia y traumatología, Ginecología y Obstetricia).</li>
					<li>Campañas de vacunación.</li>
				</ul>

				<ul class="col-sm-6">
					<li>Nutrición. Planes y seguimiento.</li> 
					<li>Homologación y visado.</li>
					<li>Gerenciamiento de servicios Médicos.</li> 
					<li>Diagnósticos por imágenes y estudios complementarios. Red de centro de  estudios complementarios. (RMN TAC ECOGRAFÍAS RX).</li>
					<li>Chequeo laboral en planta / Unidades móviles.</li> 
					<li>Adicciones.</li>
					<li>Equipo de salud Mental.</li>
				</ul>

				<div class="col-sm-5 col-sm-offset-7">
					<p class="text-right">
						<a href="{{ url('serviciosempresas/serviciomedico') }}">LEER TAMBIÉN MÉDICO Y ENFEREMERA EN PLANTA <i class="fa fa-arrow-circle-right"></i></a>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.servicios')
			</div>
		</div>
	</div>
</div>
<!-- Google Code para etiquetas de remarketing -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 941117892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/941117892/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
@endsection
