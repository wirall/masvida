@extends('main')
@section('title', '+VIDA - Eventos')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div id="carousel-eventos" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-eventos-01.png" alt="COBERTURA DE EVENTOS. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner">
							<p>COBERTURA DE EVENTOS</p>
							<p><img src="/css/img/copy-empresas.png" alt="COBERTURA DE EVENTOS. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							<a class="btn btn-primary" href="#form">COTIZAR PLAN <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>
	</div>

	<div class="row">
		<div class="intro from-bottom">
			<div class="container">
				<h2 class="text-center">COBERTURA DE EVENTOS</h2>
				<div class="row">
					<div class="col-sm-6">
						<p>Entendemos que un evento es cuando una empresa u organización realiza una reunión multitudinaria, donde juntan tanto a los concurrentes, como a su equipo de trabajo, en un mismo espacio o recinto, predio definido, o espacio público, que no tiene contratado el servicio de emergencias médicas de forma permanente.</p>
						<p>La cobertura de eventos de <strong class="red">+VIDA garantiza la atención de todo tipo de emergencia, y urgencia médica, además de la protección sanitaria de los asistentes a su evento.</strong></p>
					</div>
					<div class="col-sm-6">
						<p>Realizamos esta <strong class="red">cobertura los 365 días del año las 24 horas</strong> con una UTIM (Unidad de Terapia Intensiva Móvil), Médicos Emergentólogos y Paramédicos Entrenados en la asistencia de espectáculos en general, públicos o privados, tanto para al personal que trabaja en el lugar como también al público que asiste, para garantizar la cobertura de la salud tanto de los concurrentes como de su equipo de trabajo.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-top">
			<div class="container">
				<div class="col-sm-10 col-sm-offset-1">
					<p class="lead text-center red">JERARQUICE SU EVENTO COMERCIAL, PROTEGIENDO A LOS QUE ACUDEN A SU EVENTO, EVITANDO IMPREVISTOS E INFORTUNIOS EN SU ORGANIZACIÓN.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro from-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<p><strong class="red">+VIDA</strong> pone toda su infraestructura y su experiencia al servicio de todo tipo de eventos deportivos, sociales o comerciales, estudiando cada caso para minimizar las incidencias y prever cualquier eventualidad.<br />Nuestras unidades se encuentran equipadas con la más alta tecnología en equipamiento médico, para prestar todo el soporte técnico necesario para brindar la mayor seguridad y comodidad al paciente y cubrir de esta manera los requerimientos que necesita cada situación en particular.</p>
					</div>
					<div class="col-sm-6">
						<p>Nuestro personal está altamente calificado y en continua formación. Nuestros Médicos y paramédicos, completan su formación con cursos específicos del sector, capacitándose  especialmente en las nuevas técnicas y procedimientos utilizados en las emergencias.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary">
			<div class="container">
				<h2 class="text-center">COBERTURA PARA AMBULANCIAS UTIM<br />(UNIDAD DE TERAPIA INTENSIVA MÓVIL) PARA SU EVENTO.</h2>
				<ul class="col-sm-6 col-md-3 from-top">
					<li>Recitales</li>
					<li>Partidos de fútbol</li>
					<li>Carreras</li>
					<li>Eventos escolares</li>
				</ul>
				<ul class="col-sm-6 col-md-3 from-bottom">
					<li>Filmaciones</li>
					<li>Actos políticos</li>
					<li>Convenciones</li>
					<li>Estadios</li>
				</ul>
				<ul class="col-sm-6 col-md-3 from-top">
					<li>Presentaciones de producto</li>
					<li>Eventos empresariales</li>
					<li>Geriátricos</li>
					<li>Torneos: Rugby, Polo, Golf, etc.</li>
				</ul>
				<ul class="col-sm-6 col-md-3 from-bottom">
					<li>Maratones</li>
					<li>Grandes reuniones</li>
					<li>Exposiciones y ferias</li>
					<li>Mega eventos con varias unidades coordinadas</li>
				</ul>
				<div class="col-sm-12">
					<p class="lead red text-center from-bottom">DISEÑAMOS Y DIMENSIONAMOS LA COBERTURA A LA MEDIDA DE SU EVENTO.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro">
			<div class="container">
				<h2 class="text-center">PARA QUE SU EVENTO SEA <strong class="red">+</strong> PERFECTO</h2>
				<ul class="col-sm-6 from-left">
					<li>Móvil UTIM (unidad de terapia intensiva móvil).</li>
					<li>Rastreo satelital permanente de la unidad.</li>
					<li>Móvil dedicado y permanente durante las horas de servicio.</li>
					<li>Cláusula de reposición de la unidad. Le enviamos una segunda unidad de reemplazo.</li>
				</ul>
				<ul class="col-sm-6 from-right">
					<li>Incluye todos los gastos tanto en medicamentos como de traslados en la urgencia/emergencia.</li>
					<li>Unidad equipada con elementos de última generación.</li>
					<li>Personal médico altamente capacitado en emergentología.</li>
					<li>Paramédico capacitado para actuar en todo tipo de emergencias y urgencias médicas.</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.servicios')
			</div>
		</div>
	</div>
</div>
@endsection
