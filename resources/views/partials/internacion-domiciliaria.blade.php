@extends('main')
@section('title', '+VIDA - Internación Domiciliaria')
@section('content')
<div class="container-fluid">
	<div class="row">
		
		<div id="carousel-filosofia" class="carousel slide" data-ride="carousel">

		  	<div class="carousel-inner" role="listbox">
			    
			    <div class="item active">
					<img src="/css/img/slide-internacion-domiciliaria.png" alt="FILOSOFÍA. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS">
					<div class="carousel-caption">
						<div class="caption-inner caption-big">
							<p>INTERNACIÓN DOMICILIARIA</p>
							<p><img src="/css/img/internacion-domiciliaria.png" alt="FILOSOFÍA. DESCUBRA POR QUÉ CADA VEZ + GENTE CUENTA CON NOSOTROS" class="img-responsive"></p>
							<a class="btn btn-primary btn-big" href="#form">SOLICITAR + INFORMACIÓN <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
						</div>
					</div>
			    </div>
			    
		  	</div>

		  	<div class="left carousel-control"></div>
  			<div class="right carousel-control"></div>

		</div>
	</div>

	<div class="row">
		<div class="intro bg-grey">
			<div class="container text-center from-bottom">
				<h2>INTERNACIÓN DOMICILIARIA</h2>
				<p>La <strong >internación domiciliaria </strong>tiene como uno de sus objetivos fundamentales proporcionar la atención de diferentes profesionales de la salud en el domicilio del paciente, considerando 
sus necesidades para diseñar un plan individual de cuidados oportunos y convenientes. </p>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="intro">
			<div class="container text-center from-bottom">
				<h3 class="red">BENEFICIOS</h3>


				<div class="row">
					<div class="col-sm-6 from-left">
						<ul class="bullets">
							<li> <div class="rombo"></div> <p class="texto-rombo"> Mejora la calidad de vida y evolución del paciente al reincorporarlo a su habitualidad, hogar y familia. </p></li>
							<li> <div class="rombo"></div> <p class="texto-rombo"> Permite al grupo familiar acompañar al paciente y seguir cotidianamente los avances en su tratamiento. </p></li>
							<li> <div class="rombo"></div> <p class="texto-rombo"> Disminuyen las consecuencias de una internación hospitalaria prolongada (hospitalismo, trauma psicológico) como así también, la posibilidad de infecciones nosocomiales.</p></li>
						</ul>
					</div>
					<div class="col-sm-6 from-right">
						<ul class="bullets">
							<li> <div class="rombo"></div> <p class="texto-rombo"> Se reducen los costos para Prepagas y Obras Sociales.</p></li>
							<li> <div class="rombo"></div> <p class="texto-rombo"> Asistencia programada a todos los domicilios las 24 horas, los 365 días del año.</p></li>
							<li> <div class="rombo"></div> <p class="texto-rombo"> Idoneidad del personal de enfermería profesional que acude al domicilio.</p></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="intro panel-grey">
			<div class="container text-center from-bottom">
				<h3 class="red">Médicos Especialistas</h3>

				<div class="row col-sm-10 col-sm-offset-1">
					<div class="col-sm-3 from-left">
						<ul>
							<li>• Psiquiatras</li>
							<li>• Cardiólogos</li>
						</ul>
					</div>
					<div class="col-sm-3 from-right">
						<ul>
							<li>• Paliativistas</li>
							<li>• Neumonólogos</li>
						</ul>
					</div>
					<div class="col-sm-3 from-left">
						<ul>
							<li>• Cirujanos</li>
							<li>• Traumatólogos</li>
						</ul>
					</div>
					<div class="col-sm-3 from-right">
						<ul>
							<li>• Kinesiólogos</li>
							<li>• Nutricionistas</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-primary from-bottom">
			<div class="container">
				@include('partials.forms.servicios')
			</div>
		</div>
	</div>
</div>
</div>
@endsection
