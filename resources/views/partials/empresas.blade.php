@extends('main')
@section('title', '+VIDA - Servicios para empresas')
@section('content')
<div class="container-fluid">

	<ul class="row servicios">
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<div class="panel panel-empresas empresas">
					<img class="img-responsive" src="css/img/copy-empresas.png" alt="" />
				</div>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a class="panel panel-empresas areaprotegida" href="{{ url('serviciosempresas/areaprotegida') }}">
					<div class="caption"><span>ÁREA PROTEGIDA</span></div>
				</a>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a class="panel panel-empresas areacardio" href="{{ url('serviciosempresas/areacardio') }}">
					<div class="caption"><span>ÁREA CARDIO</span></div>
				</a>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a class="panel panel-empresas medicinalaboral" href="{{ url('serviciosempresas/medicinalaboral') }}">
					<div class="caption"><span>MEDICINA LABORAL</span></div>
				</a>
			</div>
		</li>
	</ul>

	<ul class="row servicios">
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a class="panel panel-empresas eventos" href="{{ url('serviciosempresas/eventos') }}">
					<div class="caption"><span>EVENTOS</span></div>
				</a>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a class="panel panel-empresas planes" href="{{ url('serviciosempresas/planes') }}">
					<div class="caption"><span>CONVENIO Y PLANES PARA EMPLEADOS</span></div>
				</a>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a class="panel panel-empresas serviciomedico" href="{{ url('serviciosempresas/serviciomedico') }}">
					<div class="caption"><span>SERVICIO DE MÉDICO Y ENFERMERÍA EN PLANTA</span></div>
				</a>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a class="panel panel-empresas cursos" href="{{ url('serviciosempresas/cursos') }}">
					<div class="caption"><span>CURSOS</span></div>
				</a>
			</div>
		</li>
	</ul>

</div>
</div>
@endsection