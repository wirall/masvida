@section('head-scripts')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        
        function recaptcha_callback(){
          $('#recaptcha-response').val(grecaptcha.getResponse());
          $('#submitButton').removeAttr('disabled');
        }

    </script>
@endsection

<?php 
    DEFINE('RECAPTCHA_KEY', '6LcAXkMUAAAAAOG_OYi__q6d7QLIRrD6hLH9-Pb1');
?>

@if ($errors->any())
	<script type="text/javascript">
		document.location.href = "#form";
	</script>
@endif
{!! Form::open(array('url' => 'postrrhh', 'class' => 'form', 'files'=> true)) !!}
	<a name="form"></a> 
	<h3 class="text-center red">FORMULARIO</h3>
	<div class="col-sm-10 col-sm-offset-1">
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('nombre')) has-error @endif">
				    {!! Form::label('nombre','NOMBRE') !!}
				    {!! Form::input('text','nombre', null, array('class'=>'form-control', 'placeholder'=>'')) !!}
				     @if ($errors->has('nombre')) <p class="help-block">{{ $errors->first('nombre') }}</p> @endif
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('apellido')) has-error @endif">
					{!! Form::label('apellido','APELLIDO') !!}
				    {!! Form::input('text','apellido', null, array('class'=>'form-control', 'placeholder'=>'')) !!}
				     @if ($errors->has('apellido')) <p class="help-block">{{ $errors->first('apellido') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group @if ($errors->has('puesto')) has-error @endif">
				    {!! Form::label('puesto','PUESTO AL QUE DESEARÍA APLICAR') !!}
				    {!! Form::input('text','puesto', null, array('class'=>'form-control', 'placeholder'=>'')) !!}
				     @if ($errors->has('puesto')) <p class="help-block">{{ $errors->first('puesto') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group @if ($errors->has('departamento')) has-error @endif">
				    {!! Form::label('departamento','DEPARTAMENTO AL QUE DESEARÍA APLICAR') !!}
				    {!! Form::input('text','departamento', null, array('class'=>'form-control', 'placeholder'=>'')) !!}
				     @if ($errors->has('departamento')) <p class="help-block">{{ $errors->first('departamento') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('direccion')) has-error @endif">
					{!! Form::label('direccion','DIRECCIÓN') !!}
				    {!! Form::input('text','direccion', null, array('class'=>'form-control', 'placeholder'=>'')) !!}
				     @if ($errors->has('direccion')) <p class="help-block">{{ $errors->first('direccion') }}</p> @endif
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('localidad')) has-error @endif">
				    {!! Form::label('localidad','LOCALIDAD') !!}
				    {!! Form::input('text','localidad', null, array('class'=>'form-control', 'placeholder'=>'')) !!}
				     @if ($errors->has('localidad')) <p class="help-block">{{ $errors->first('localidad') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('telefono')) has-error @endif">
				    {!! Form::label('telefono','TELÉFONO') !!}
				    {!! Form::input('text','telefono', null, array('class'=>'form-control', 'placeholder'=>'')) !!}
				     @if ($errors->has('telefono')) <p class="help-block">{{ $errors->first('telefono') }}</p> @endif
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('email')) has-error @endif">
					{!! Form::label('email','EMAIL') !!}
				    {!! Form::input('text','email', null, array('class'=>'form-control', 'placeholder'=>'')) !!}
				     @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group @if ($errors->has('matriculado')) has-error @endif">
					{!! Form::label('matricula','¿ES PROFESIONAL MATRICULADO?') !!} <br />
				  {!! Form::radio('matriculado','si') !!} Si&nbsp; &nbsp; 
					{!! Form::radio('matriculado','no') !!} No
					@if ($errors->has('matriculado')) <p class="help-block">{{ $errors->first('matriculado') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group @if ($errors->has('carta')) has-error @endif">
					{!! Form::label('carta','CARTA DE PRESENTACIÓN') !!}
				    {!! Form::textarea('carta', null, array('class'=>'form-control', 'placeholder'=>'')) !!}
				     @if ($errors->has('carta')) <p class="help-block">{{ $errors->first('carta') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<p><strong>POR FAVOR ADJUNTE UNA COPIA ACTUALIZADA DE SU C.V. PROFESIONAL.</strong></p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group @if ($errors->has('cv')) has-error @endif">
					{!! Form::file('cv', array('class'=>'filestyle', 'data-icon'=>'false', 'data-buttonText'=>'Examinar')) !!}
				     @if ($errors->has('cv')) <p class="help-block">{{ $errors->first('cv') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
            <div class="col-sm-6" style="margin-bottom:20px">
                <div id="g-recaptcha" class="g-recaptcha" data-sitekey="{{ RECAPTCHA_KEY }}" data-callback="recaptcha_callback" data-expired-callback="recaptcha_expired_callback"></div>
                {!! Form::hidden('recaptcha-response', null, ['id' => 'recaptcha-response']) !!}                
            </div>  
			<div class="col-sm-6 text-right">
				<div class="form-group">
			    	{!! Form::submit('ENVIAR', array('class'=>'btn btn-success', 'id' => 'submitButton', 'disabled' => 'disabled')) !!}
				</div>
			</div>
		</div>

{!! Form::close() !!}

