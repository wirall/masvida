@section('head-scripts')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        
        function recaptcha_callback(){
          $('#recaptcha-response').val(grecaptcha.getResponse());
          $('#submitButton').removeAttr('disabled');
        }

    </script>
@endsection

<?php 
    DEFINE('RECAPTCHA_KEY', '6LcAXkMUAAAAAOG_OYi__q6d7QLIRrD6hLH9-Pb1');
?>

@if ($errors->any())
	<script type="text/javascript">
		document.location.href = "#form";
	</script>
@endif


{!! Form::open(array('url' => Request::path() . 'post', 'class' => 'form')) !!}
	<a name="form"></a> 
	<h3 class="text-center red">FORMULARIO</h3>
	<div class="col-sm-10 col-sm-offset-1">

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('empresa')) has-error @endif">
				    {!! Form::label('empresa','NOMBRE DE LA EMPRESA') !!}
				    {!! Form::input('text','empresa', null, array( 'class'=>'form-control', 'placeholder'=>'')) !!}
				    @if ($errors->has('empresa')) <p class="help-block">{{ $errors->first('empresa') }}</p> @endif
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('cargo')) has-error @endif">
					{!! Form::label('cargo','CARGO') !!}
				    {!! Form::input('text','cargo', null, array( 'class'=>'form-control', 'placeholder'=>'')) !!}
				    @if ($errors->has('cargo')) <p class="help-block">{{ $errors->first('cargo') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('nombre')) has-error @endif">
				    {!! Form::label('nombre','NOMBRE', array('class' => 'required')) !!}
				    {!! Form::input('text','nombre', null, array( 'class'=>'form-control', 'placeholder'=>'')) !!}
				    @if ($errors->has('nombre')) <p class="help-block">{{ $errors->first('nombre') }}</p> @endif
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('apellido')) has-error @endif">
					{!! Form::label('apellido','APELLIDO', array('class' => 'required')) !!}
				    {!! Form::input('text','apellido', null, array( 'class'=>'form-control', 'placeholder'=>'')) !!}
				    @if ($errors->has('apellido')) <p class="help-block">{{ $errors->first('apellido') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('telefono')) has-error @endif">
				    {!! Form::label('telefono','TELÉFONO', array('class' => 'required')) !!}
				    {!! Form::input('text','telefono', null, array( 'class'=>'form-control', 'placeholder'=>'')) !!}
				    @if ($errors->has('telefono')) <p class="help-block">{{ $errors->first('telefono') }}</p> @endif
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('email')) has-error @endif">
					{!! Form::label('email','EMAIL', array('class' => 'required')) !!}
				    {!! Form::input('text','email', null, array( 'class'=>'form-control', 'placeholder'=>'')) !!}
				    @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('localidad')) has-error @endif">
				    {!! Form::label('localidad','LOCALIDAD') !!}
				    {!! Form::input('text','localidad', null, array( 'class'=>'form-control', 'placeholder'=>'')) !!}
				    @if ($errors->has('localidad')) <p class="help-block">{{ $errors->first('localidad') }}</p> @endif
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('cantidad')) has-error @endif">
					{!! Form::label('cantidad','CANTIDAD DE EMPLEADOS') !!}
				    {!! Form::input('text','cantidad', null, array( 'class'=>'form-control', 'placeholder'=>'')) !!}
				    @if ($errors->has('cantidad')) <p class="help-block">{{ $errors->first('cantidad') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group @if ($errors->has('rubro')) has-error @endif">
					{!! Form::label('rubro','RUBRO DE LA EMPRESA') !!}
				    {!! Form::input('text','rubro', null, array( 'class'=>'form-control', 'placeholder'=>'')) !!}
				    @if ($errors->has('rubro')) <p class="help-block">{{ $errors->first('rubro') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group @if ($errors->has('comentarios')) has-error @endif">
					{!! Form::label('comentarios','COMENTARIOS') !!}
				    {!! Form::textarea('comentarios', null, array( 'class'=>'form-control', 'placeholder'=>'')) !!}
				    @if ($errors->has('comentarios')) <p class="help-block">{{ $errors->first('comentarios') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group @if ($errors->has('info')) has-error @endif">
					{!! Form::checkbox('info',null) !!} No deseo recibir información de promociones.
					@if ($errors->has('info')) <p class="help-block">{{ $errors->first('info') }}</p> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<span class="required"></span> Campos requeridos 
				</div>
			</div>
		</div>

		<div class="row">
            <div class="col-sm-6" style="margin-bottom:20px">
                <div id="g-recaptcha" class="g-recaptcha" data-sitekey="{{ RECAPTCHA_KEY }}" data-callback="recaptcha_callback" data-expired-callback="recaptcha_expired_callback"></div>
                {!! Form::hidden('recaptcha-response', null, ['id' => 'recaptcha-response']) !!}                
            </div>  
			<div class="col-sm-6 text-right">
				<div class="form-group">
			    	{!! Form::submit('ENVIAR', array('class'=>'btn btn-success', 'id' => 'submitButton', 'disabled' => 'disabled')) !!}
				</div>
			</div>
		</div>

{!! Form::close() !!}
