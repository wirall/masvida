@extends('main')

@section('title', '+VIDA - Gracias')

@section('content')

	<div class="container-fluid">
		<div class="row">
			<div id="carousel-home" class="carousel slide" data-ride="carousel">
	  		
	  			<!-- Indicators -->
	  			<ol class="carousel-indicators">
	    			<li data-target="#carousel-home" data-slide-to="0" class="active"></li>
	    			<li data-target="#carousel-home" data-slide-to="1"></li>
	    			<li data-target="#carousel-home" data-slide-to="2"></li>
	    			<li data-target="#carousel-home" data-slide-to="3"></li>
	  			</ol>

	  			<!-- Wrapper for slides -->
			  	<div class="carousel-inner" role="listbox">
				    <div class="item active">
						<img src="/css/img/slide-home-01.png" alt="RECURSOS HUMANOS. TRABAJAR CON NOSOTROS Y DAR MÁS VIDA">
						<div class="carousel-caption">
							<div class="caption-inner">
								<p>RECURSOS HUMANOS</p>
								<h1>TRABAJAR</h1>
								<h3>CON NOSOTROS</h3>
								<h2>Y DAR MÁS VIDA</h2>
								<a class="btn btn-primary" href="{{ url('rrhh') }}">MÁS INFO <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
							</div>
						</div>
				    </div>
					<div class="item">
						<img src="/css/img/slide-home-02.png" alt="EMPRESAS. BIENESTAR, ASESORAMIENTO Y SERVICIOS MÁS VIDA">
						<div class="carousel-caption">
							<div class="caption-inner">
								<p>EMPRESAS</p>
								<h1>BIENESTAR</h1>
								<h3>ASESORAMIENTO Y SERVICIOS <strong>MÁS VIDA</strong></h3>
								<a class="btn btn-primary" href="{{ url('empresas') }}">MÁS INFO <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
							</div>
						</div>
				    </div>
				    <div class="item">
						<img src="/css/img/slide-home-03.png" alt="FILOSOFIA. TRABAJAMOS Y NOS COMPROMETEMOS PARA DAR MÁS VIDA">
						<div class="carousel-caption">
							<div class="caption-inner">
								<p>FILOSOFIA</p>
								<h1>TRABAJAMOS</h1>
								<h3>Y NOS COMPROMETEMOS PARA <strong>DAR MÁS VIDA</strong></h3>
								<a class="btn btn-primary" href="{{ url('filosofia') }}">MÁS INFO <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
							</div>
						</div>
				    </div>
				    <div class="item">
						<img src="/css/img/slide-home-04.png" alt="FAMILIA. PROTEGER A LOS TUYOS ES DAR MÁS VIDA">
						<div class="carousel-caption">
							<div class="caption-inner">
								<p>FAMILIA</p>
								<h1>PROTEGER</h1>
								<h3>A LOS TUYOS ES DAR</h3>
								<h2>MÁS VIDA</h2>
								<a class="btn btn-primary" href="{{ url('familia') }}">MÁS INFO <img src="/css/img/chevron-transparent.png" alt="MÁS INFO"></a>
							</div>
						</div>
				    </div>
			  	</div>

	  			<!-- Controls -->
	  			<a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev">
	    			<span class="chevron-left" aria-hidden="true">Anterior</span>
	  			</a>
	  			<a class="right carousel-control" href="#carousel-home" role="button" data-slide="next">
	    			<span class="chevron-right" aria-hidden="true">Siguiente</span>
	  			</a>

			</div>
		</div>

		<div class="row">
			<h3 class="text-center">Gracias por comunicarse con <strong class="red">+ VIDA</strong></h3>
			<div class="col-xs-12 text-center">
				<div class="form-group">
					<a class="btn btn-primary" href="{{ url('/')}}">Volver a la home</a>
				</div>
			</div>
		</div>
	</div>

@endsection