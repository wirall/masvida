
@extends('auth.main')

@section('content')

	<div class="container-fluid title-section">
		<h2 class="text-center text-uppercase"> <span><img src="/css/img/logo.png" height="40" width="auto"> Iniciar sesión</span></h2>
		<hr>
	</div>

	<div class="container">

			<div class="col-md-8 col-md-offset-2">

				<form class="login form-horizontal" role="form" method="POST" action="/auth/login">

					{!! csrf_field() !!}
					
					{{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}

					<div class="form-group @if ($errors->has('email')) has-error @endif">
						<label class="col-md-4 control-label">Email</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="email" value="{{ old('email') }}">
							@if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
						</div>
					</div>

					<div class="form-group @if ($errors->has('password')) has-error @endif">
						<label class="col-md-4 control-label">Password</label>
						<div class="col-md-6">
							<input type="password" class="form-control" name="password">
							@if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
						</div>
					</div>

					<br>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<input type="checkbox" name="remember"> Recordar
						</div>
					</div>

					<br>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-login-submit" style="margin-right: 15px;">
								Login
							</button>
						</div>
					</div>

				
				</form>

			</div>

	</div>
@endsection
