@extends('auth.main')

@section('content')

	<div class="container-fluid title-section">
		<h2 class="text-center text-uppercase"> <span><img src="/css/img/logo.png" height="40" width="auto"> Resetear password</span></h2>
		<hr>
	</div>

	<div class="container">

			<div class="col-md-8 col-md-offset-2">

				<form class="login form-horizontal" role="form" method="POST" action="/password/email">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						<label class="col-md-4 control-label">Email</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="email" value="{{ old('email') }}">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-login-submit" style="margin-right: 15px;">
								Resetear Password
							</button>
						</div>
					</div>

				</form>

			</div>

	</div>
@endsection
