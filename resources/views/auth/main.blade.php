<!DOCTYPE html>
<html lang="en" class="login">
      <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=9;IE=10;IE=Edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="Template">
        <meta name="author" content="Wirall Interactive">

        <title>@yield("title", "Mas Vida - Backend")</title>
        <!-- Favicon -->
        <link rel="icon" href="{{ asset('/favicon.png') }}" type="image/png" sizes="16x16">
        <!-- Vendors -->
        <link rel="stylesheet" href="{{ asset('/css/vendors.css') }}">
        <!-- Custom -->
        <link href="{{ asset('/css/backend/app.css') }}" rel="stylesheet">

    </head>

  <body >

    @yield("content")

    <!-- Scripts -->
    <script src="{{ asset('js/vendors.js') }}"></script>
    <script src="{{ asset('js/backend/app.js') }}"></script>
    @yield("viewscripts")

  </body>


</html>
