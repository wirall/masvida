<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">

        <link href="{{ asset('css/vendors.css') }}" rel="stylesheet">
        <link href="{{ asset('css/frontend/app.css') }}" rel="stylesheet">

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        @yield('head-scripts')
    </head>
    <body>

        <div class="mask">
            <div id="loader"></div>
        </div>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav">
                        <span class="sr-only">Navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}"><img src="/css/img/logo.png" alt="+VIDA"></a>
                </div>

                <div class="navbar-left slogan">
                    CONTÁCTENOS <img class="img-responsive" src="/css/img/contactenos.png" alt="CONTÁCTENOS 4725-6600 * 0800-888-8432 (VIDA)">
                </div>

                <nav class="collapse navbar-collapse" id="nav">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="{{Request::path() == '/' ? 'active' : ''}}"><a href="{{ url('/') }}">EMPRESAS</a></li>
                        <li class="{{Request::path() == 'familia' ? 'active' : ''}}"><a href="{{ url('familia') }}">FAMILIA</a></li>
                        <li class="{{Request::path() == 'internacion-domiciliaria' ? 'active' : ''}}"><a href="{{ url('internacion-domiciliaria') }}">INTERNACION DOMICILIARIA</a></li>
                        <li class="{{Request::path() == 'novedades' || Request::path() == 'novedades/*' ? 'active' : ''}}"><a href="{{ url('novedades') }}">NOVEDADES</a></li>
                        <li class="{{Request::path() == 'filosofia' ? 'active' : ''}}"><a href="{{ url('filosofia') }}">FILOSOFÍA</a></li>
                        <li class="{{Request::path() == 'rrhh' ? 'active' : ''}}"><a href="{{ url('rrhh') }}">RECURSOS HUMANOS</a></li>
                        <!-- <li class="dropdown {{Request::path() == 'familia' || Request::path() == 'empresas' ? 'active' : ''}}">
                        <a href="{{ url('servicios') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">SERVICIOS <span class="caret red"></span></a>
                        <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('familia') }}">FAMILIA</a></li>
                                        <li><a href="{{ url('empresas') }}">EMPRESAS</a></li>
                                </ul>
                        </li> -->
                        <li><a class="social" href="https://www.linkedin.com/company/mas-vida" target="_blank"><img src="/css/img/icon-linkedin.png" alt="LinkedIn"></a></li>
                    </ul>
                </nav>
            </div>
        </nav>

        @yield('content')

        @if(Request::path() == '/' || Request::path() == 'empresas' || Request::path() == 'novedades' || Request::path() != 'novedad/*')
        <footer class="footer">
            <div class="info container">
                <div class="col-sm-6">
                    <ul class="row">
                        <li class="col-sm-5">EMERGENCIAS MÉDICAS</li>
                        <li class="col-sm-7"><strong>0810-444-8432 (VIDA) &bull; 011 4725-6666</strong></li>
                    </ul>
                    <ul class="row">
                        <li class="col-sm-5">CONSULTAS MÉDICAS</li>
                        <li class="col-sm-7"><strong>011 4725-6660</strong></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul class="row">
                        <li>COMERCIAL</li>
                        <li><strong>0800-888-8432 (VIDA)</strong></li>
                    </ul>
                    <ul class="row">
                        <li>ADMINISTRACIÓN Y CONSULTAS GENERALES</li>
                        <li><strong>011 4725-6600</strong></li>
                        <li><a href="mailto:contacto@masvida.com.ar">contacto@masvida.com.ar</a></li>
                    </ul>
                </div>
            </div>
            <div class="subfooter">
                <div class="copyright container">
                    <div class="col-md-6 col-md-offset-6">
                        <div class="row">
                            <p>&copy; Copyright 2015 - All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        @else
    </div>
    <div class="container">
        <div class="row text-center from-bottom">
            <div class="col-md-12">
                <div class="footer-clientes">	
                    <h2 class="h2-clientes">ALGUNOS DE NUESTROS CLIENTES</h2>
                    <br>
                    <br>
                    <div id="clientes">
                        <div><img src="/css/img/footer-cliente-1.png"></div>
                        <div><img src="/css/img/footer-cliente-2.png"></div>
                        <div><img src="/css/img/footer-cliente-3.png"></div>
                        <div><img src="/css/img/footer-cliente-4.png"></div>
                        <div><img src="/css/img/footer-cliente-5.png"></div>
                        <div><img src="/css/img/footer-cliente-6.png"></div>
                    </div>
                </div>	
            </div>
        </div>
        <hr>
        <div class="row text-center from-bottom">
            <div class="col-md-12">
                <div class="footer-clientes">
                    <h2>ÁREAS PROTEGIDAS Y MEDICINA LABORAL</h2>
                    <br>
                    <br>
                    <div id="areas">
                        <div><img src="/css/img/footer-areas-1.png"></div>
                        <div><img src="/css/img/footer-areas-2.png"></div>
                        <div><img src="/css/img/footer-areas-3.png"></div>
                        <div><img src="/css/img/footer-areas-4.png"></div>
                        <div><img src="/css/img/footer-areas-5.png"></div>
                        <div><img src="/css/img/footer-areas-6.png"></div>
                        <div><img src="/css/img/footer-areas-7.png"></div>
                        <div><img src="/css/img/footer-areas-8.png"></div>
                        <div><img src="/css/img/footer-areas-9.png"></div>
                    </div>
                </div>
            </div>
        </div>	
    </div>
    <footer class="footer">
        <div class="info container">
            <div class="col-sm-6">
                <ul class="row">
                    <li class="col-sm-5">EMERGENCIAS MÉDICAS</li>
                    <li class="col-sm-7"><strong>0810-444-8432 (VIDA) &bull; 011 4725-6666</strong></li>
                </ul>
                <ul class="row">
                    <li class="col-sm-5">CONSULTAS MÉDICAS</li>
                    <li class="col-sm-7"><strong>011 4725-6660</strong></li>
                </ul>
            </div>
            <div class="col-sm-6">
                <ul class="row">
                    <li>COMERCIAL</li>
                    <li><strong>0800-888-8432 (VIDA)</strong></li>
                </ul>
                <ul class="row">
                    <li>ADMINISTRACIÓN Y CONSULTAS GENERALES</li>
                    <li><strong>011 4725-6600</strong></li>
                    <li><a href="mailto:contacto@masvida.com.ar">contacto@masvida.com.ar</a></li>
                </ul>
            </div>
        </div>
        <div class="subfooter">
            <div class="copyright container">
                <div class="col-md-6 col-md-offset-6">
                    <div class="row">
                        <p>&copy; Copyright 2015 - All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>		
    @endif

    <!-- Scripts -->
    <script src="{{ asset('js/vendors.js') }}"></script>
    <script src="{{ asset('js/frontend/app.js') }}"></script>
    @yield("viewscripts")

    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-66340577-1', 'auto');
        ga('send', 'pageview');
    </script>
    @if(Request::path() == 'filosofiapost')
    <!-- Google Code for Formulario Filosof&iacute;a Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "urfOCLmyn18QxKPhwAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941117892/?label=urfOCLmyn18QxKPhwAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    @elseif(Request::path() == 'postfamilia')
    <!-- Google Code for Formulario Familia Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "t_V3CNyKlF8QxKPhwAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941117892/?label=t_V3CNyKlF8QxKPhwAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    @elseif(Request::path() == 'serviciosempresas/cursospost')
    <!-- Google Code for Formulario Cursos Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "SSbvCMywn18QxKPhwAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941117892/?label=SSbvCMywn18QxKPhwAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    @elseif(Request::path() == 'serviciosempresas/serviciomedicopost')
    <!-- Google Code for Formulario Servicio de M&eacute;dico y Enfermer&iacute;a en Planta Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "KeIsCOeTmV8QxKPhwAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941117892/?label=KeIsCOeTmV8QxKPhwAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    @elseif(Request::path() == 'serviciosempresas/planespost')
    <!-- Google Code for Formulario Convenio y Planes para Empleados Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "5M1nCKmun18QxKPhwAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941117892/?label=5M1nCKmun18QxKPhwAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    @elseif(Request::path() == 'serviciosempresas/eventospost')
    <!-- Google Code for Formulario Eventos Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "V5Z4CKyKlF8QxKPhwAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941117892/?label=V5Z4CKyKlF8QxKPhwAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    @elseif(Request::path() == 'serviciosempresas/areaprotegidapost')
    <!-- Google Code for Formulario &Aacute;rea Protegida Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "JFSQCLyIlF8QxKPhwAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941117892/?label=JFSQCLyIlF8QxKPhwAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    @elseif(Request::path() == 'serviciosempresas/areacardiopost')
    <!-- Google Code for Formulario &Aacute;rea Cardio Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "cTVvCPKIlF8QxKPhwAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941117892/?label=cTVvCPKIlF8QxKPhwAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    @elseif(Request::path() == 'serviciosempresas/medicinalaboralpost')
    <!-- Google Code for Formulario Medicina Laboral Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "8L4DCLOsn18QxKPhwAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941117892/?label=8L4DCLOsn18QxKPhwAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    @elseif(Request::path() == 'postrrhh')
    <!-- Google Code for Formulario RRHH Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "mZbMCPudn18QxKPhwAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941117892/?label=mZbMCPudn18QxKPhwAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    @endif
    <!-- Google Code para etiquetas de remarketing -->
    <!-- 
        Es posible que las etiquetas de remarketing todavía no estén asociadas a la información de identificación personal o que estén en 
        páginas relacionadas con las categorías delicadas. Para obtener más información e instrucciones sobre cómo configurar 
        la etiqueta, consulte http://google.com/ads/remarketingsetup.
    -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 941117892;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/941117892/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
    

</body>
</html>
