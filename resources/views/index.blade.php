@extends('main')
@section('title', '+VIDA - Servicios para empresas')
@section('content')
<div class="container-fluid">

	<ul class="row servicios">
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<div class="panel panel-empresas empresas">
					<img class="img-responsive" src="css/img/copy-empresas.png" alt="" />
				</div>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a href="{{ url('serviciosempresas/areaprotegida') }}" class="hidden-lg">
					<div class="panel panel-empresas areaprotegida">
						<div class="caption-small">
							<span>ÁREA PROTEGIDA</span>
						</div>
					</div>
				</a>
				<div class="panel panel-empresas areaprotegida hidden-sm hidden-md hidden-xs" >
					<div class="caption"><span>ÁREA PROTEGIDA</span>
						<div class="empresa-text text-center">
							<h4>Su Empresa o Negocio protegido las 24 horas, los 365 días del año.</h4>
							<a href="{{ url('serviciosempresas/areaprotegida') }}"><h3 class="know">INFORMACIÓN</h3></a>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a href="{{ url('serviciosempresas/areacardio') }}" class="hidden-lg">
					<div class="panel panel-empresas areacardio">
						<div class="caption-small">
							<span>ÁREA CARDIO</span>
						</div>
					</div>
				</a>
				<div class="panel panel-empresas areacardio hidden-sm hidden-md hidden-xs">
					<div class="caption"><span>ÁREA CARDIO</span>
						<div class="empresa-text text-center">
							<h4>Certificación en prevención en casos de riesgo de vida ante una muerte súbita.</h4>
							<a href="{{ url('serviciosempresas/areacardio') }}"><h3 class="know">INFORMACIÓN</h3></a>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a href="{{ url('serviciosempresas/medicinalaboral') }}" class="hidden-lg">
					<div class="panel panel-empresas medicinalaboral">
						<div class="caption-small">
							<span>MEDICINA LABORAL</span>
						</div>
					</div>
				</a>
				<div class="panel panel-empresas medicinalaboral hidden-sm hidden-md hidden-xs">
					<div class="caption"><span>MEDICINA LABORAL</span>
						<div class="empresa-text text-center">
							<h4>Exámenes de Salud, Control de Ausentismo, Chequeos Ejecutivos y más.</h4>
							<a href="{{ url('serviciosempresas/medicinalaboral') }}"><h3 class="know">INFORMACIÓN</h3></a>
						</div>
					</div>
				</div>
			</div>
		</li>
	</ul>

	<ul class="row servicios">
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a href="{{ url('serviciosempresas/eventos') }}" class="hidden-lg">
					<div class="panel panel-empresas eventos">
						<div class="caption-small">
							<span>EVENTOS</span>
						</div>
					</div>
				</a>
				<div class="panel panel-empresas eventos hidden-sm hidden-md hidden-xs">
					<div class="caption"><span>EVENTOS</span>
						<div class="empresa-text text-center">
							<h4>Jerarquice su evento comercial y prevenga accidentes.</h4>
							<a href="{{ url('serviciosempresas/eventos') }}"><h3 class="know">INFORMACIÓN</h3></a>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a href="{{ url('serviciosempresas/planes') }}" class="hidden-lg">
					<div class="panel panel-empresas planes">
						<div class="caption-small">
							<span>CONVENIO Y PLANES PARA EMPLEADOS</span>
						</div>
					</div>
				</a>
				<div class="panel panel-empresas planes  hidden-sm hidden-md hidden-xs">
					<div class="caption"><span>CONVENIO Y PLANES PARA EMPLEADOS</span>
						<div class="empresa-text text-center">
							<h4>Otorgue beneficios adicionales para sus Empleados.</h4>
							<a href="{{ url('serviciosempresas/planes') }}"><h3 class="know">INFORMACIÓN</h3></a>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a href="{{ url('serviciosempresas/serviciomedico') }}" class="hidden-lg">
					<div class="panel panel-empresas serviciomedico">
						<div class="caption-small">
							<span>SERVICIO DE MÉDICO Y ENFERMERÍA EN PLANTA</span>
						</div>
					</div>
				</a>
				<div class="panel panel-empresas serviciomedico hidden-sm hidden-md hidden-xs">
					<div class="caption"><span>SERVICIO DE MÉDICO Y ENFERMERÍA EN PLANTA</span>
						<div class="empresa-text text-center">
							<h4>Asegure la Cobertura Médica requerida por ley.</h4>
							<a href="{{ url('serviciosempresas/serviciomedico') }}"><h3 class="know">INFORMACIÓN</h3></a>
						</div>
					</div>	
				</div>
			</div>
		</li>
		<li class="col-sm-6 col-md-3">
			<div class="row">
				<a href="{{ url('serviciosempresas/cursos') }}" class="hidden-lg">
					<div class="panel panel-empresas cursos">
						<div class="caption-small">
							<span>CURSOS</span>
						</div>
					</div>
				</a>
				<div class="panel panel-empresas cursos hidden-sm hidden-md hidden-xs">
					<div class="caption"><span>CURSOS</span>
						<div class="empresa-text text-center">
							<h4>Educamos y capacitamos a la comunidad en temas de salud. Cursos RCP.</h4>
							<a href="{{ url('serviciosempresas/cursos') }}"><h3 class="know">INFORMACIÓN</h3></a>
						</div>
					</div>	
				</div>
			</div>
		</li>
	</ul>

</div>
@endsection