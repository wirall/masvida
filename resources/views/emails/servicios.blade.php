<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	
	<table border="0" align="center" cellpadding="0" cellspacing="0" width="600">
		<thead>
			<tr>
				<th align="left"><img src="<?php echo Request::root() . '/css/img/logo.png'; ?>" width="82" height="82" alt="" /></th>
				<th>Contacto Servicios</th>
			</tr>
			<tr>
				<th colspan="2" height="4" bgcolor="#f7001a"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><b>NOMBRE DE LA EMPRESA</b></td>
				<td><?php echo $empresa; ?></td>
			</tr>
			<tr>
				<td><b>CARGO</b></td>
				<td><?php echo $cargo; ?></td>
			</tr>
			<tr>
				<td><b>NOMBRE</b></td>
				<td><?php echo $nombre; ?></td>
			</tr>
			<tr>
				<td><b>APELLIDO</b></td>
				<td><?php echo $apellido; ?></td>
			</tr>
			<tr>
				<td><b>TELÉFONO</b></td>
				<td><?php echo $telefono; ?></td>
			</tr>
			<tr>
				<td><b>EMAIL</b></td>
				<td><?php echo $email; ?></td>
			</tr>
			<tr>
				<td><b>LOCALIDAD</b></td>
				<td><?php echo $localidad; ?></td>
			</tr>
			<tr>
				<td><b>CANTIDAD DE EMPLEADOS</b></td>
				<td><?php echo $cantidad; ?></td>
			</tr>
			<tr>
				<td><b>RUBRO DE LA EMPRESA</b></td>
				<td><?php echo $rubro; ?></td>
			</tr>
			<tr>
				<td><b>COMENTARIOS</b></td>
				<td><?php echo $comentarios; ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</tbody>
		<tfoot bgcolor="#363636">
			<tr>
				<td colspan="2" height="40"></td>
			</tr>
		</tfoot>
	</table>

</body>
</html>