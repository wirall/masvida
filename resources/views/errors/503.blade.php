<!DOCTYPE html>
<html>
  <head>
    <title>Mantenimiento</title>

		<link rel="icon" href="{{ asset('css/front/img/favicon.png') }}" type="image/png" sizes="16x16">  
		<!-- Vendors -->
		<link rel="stylesheet" href="{{ asset('css/vendors.css') }}">
		<!-- Custom -->
		<link href="{{ asset('css/front.css') }}" rel="stylesheet">

    <style>
      html, body {
        height: 100%;
      }
      body {
        display: table;
        font-size: 16px;
        padding-bottom: 0px;
        width: 100%;
      }
      p {
        font-size: 1em;
        margin: 0.5em 0;
      }
      p a, p a:hover, p a:active, p a:focus {
      	color: #000;
      }
      .container {
        text-align: center;
        display: table-cell;
        vertical-align: middle;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="content">
      	<img src="/css/img/logo.png" width="auto" height="80">
      	<hr>
        <h3>Modo Mantenimiento.</h3>
      	<p>Estamos trabajando para mejorar tu experiencia.</p>
      	<p>Te pedimos disculpas por los inconvenientes ocasionados.</p>
      	<p>Volvé a intentar en unos minutos.</p>
      	<hr>
      </div>
    </div>
  </body>
</html>
