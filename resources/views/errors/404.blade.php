<html>
	<head>
		<link href="http://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900" rel="stylesheet" type="text/css">
		<style>
			body {
				background-color: #f7001a;
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #fff;
				display: table;
				font-weight: 100;
				font-family: 'Raleway';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">URL inexistente</div>
			</div>
		</div>
		<script type="text/javascript">
			function redirect() {
        window.location.href = '/';
      }
			window.onload = redirect;
		</script>
	</body>
</html>
