var gulp 			= require('gulp');
var elixir 		= require('laravel-elixir');
var uglify 		= require('gulp-uglify');
var minifycss = require('gulp-minify-css');
//var image 		= require('gulp-image');

elixir(function (mix) {
    mix

    .styles([
      './resources/assets/bower/bootstrap/dist/css/bootstrap.css',
			'./resources/assets/bower/font-awesome/css/font-awesome.css',
			'./resources/assets/bower/slick-carousel/slick/slick.css',
			'./resources/assets/bower/slick-carousel/slick/slick-theme.css',
      './resources/assets/bower/bootstrap-select/dist/css/bootstrap-select.css',
      './resources/assets/bower/jasny-bootstrap/dist/css/jasny-bootstrap.css',
      './resources/assets/bower/datatables.net-bs/css/dataTables.bootstrap.css',
      './resources/assets/bower/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
    ], "public/css/vendors.css")

    .sass([
        './resources/assets/sass/backend/app.sass'
    ], "public/css/backend/app.css")

    .sass([
        './resources/assets/sass/frontend/app.sass'
    ], "public/css/frontend/app.css")

    .scripts([
        './resources/assets/bower/jquery/dist/jquery.js',
        './resources/assets/bower/bootstrap/dist/js/bootstrap.js',
        './resources/assets/bower/bootstrap-sass/assets/javascripts/bootstrap.js',
        './resources/assets/bower/bootstrap-filestyle/src/bootstrap-filestyle.js',
        './resources/assets/bower/slick-carousel/slick/slick.js',
        './resources/assets/bower/jquery.appear/jquery.appear.js',
        './resources/assets/bower/datatables.net/js/jquery.dataTables.js',
        './resources/assets/bower/datatables.net-bs/js/dataTables.bootstrap.js',
        './resources/assets/bower/datatables.net-responsive/js/dataTables.responsive.min.js',
        './resources/assets/bower/datatables.net-responsive-bs/js/responsive.bootstrap.js',
        './resources/assets/bower/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',
        './resources/assets/bower/bootstrap-select/dist/js/bootstrap-select.js',
        './resources/assets/bower/bootpag/lib/jquery.bootpag.js',
        './resources/assets/bower/tinymce-wirall/tinymce.js',
    ], "public/js/vendors.js")

    .scripts('./resources/assets/js/frontend/app.js', 'public/js/frontend/app.js')
    .scripts('./resources/assets/js/frontend/noticias.js', "public/js/frontend/noticias.js")
    .scripts('./resources/assets/js/backend/app.js', "public/js/backend/app.js")
    .scripts('./resources/assets/js/backend/noticias.js', "public/js/backend/noticias.js")
    .scripts('./resources/assets/js/backend/tinymce-init.js', "public/js/backend/tinymce-init.js")
    .scripts('./resources/assets/js/backend/landings.js', "public/js/backend/landings.js")


    .copy('./resources/assets/*.png', 'public')
    .copy('./resources/assets/img', 'public/css/img')
    .copy('./resources/assets/fonts/', 'public/css/fonts')
    .copy('./resources/assets/bower/font-awesome/fonts/', 'public/css/fonts')
    .copy('./resources/assets/bower/bootstrap/fonts/', 'public/fonts')
    .copy('./resources/assets/bower/tinymce-wirall/plugins/', 'public/js/plugins')
    .copy('./resources/assets/bower/tinymce-wirall/themes/', 'public/js/themes')
    .copy('./resources/assets/bower/tinymce-wirall/skins/', 'public/js/skins')

    .task('minify')

});


// Minify All
gulp.task('minify', function () {

	console.log('Minificando...');
  
  gulp.src('public/js/*.js')
  .pipe(uglify())
  .pipe(gulp.dest('public/js/'))

  gulp.src('public/css/*.css')
  .pipe(minifycss())
  .pipe(gulp.dest('public/css/'))

  gulp.src('public/css/img/**/')
  //.pipe(image())
  .pipe(gulp.dest('public/css/img/'));

});



